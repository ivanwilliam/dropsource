import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
from utility_funcs import *
from collections import Counter

class param_funcs:
        
        #variables for this class

        label_to_find = 'api_description'                    
	
        def extract_prev_siblings(self,child,maxsibs=5):
                # lets get the first 3 prev and next siblings
                numsib = 0
                psibs=[]
                for sib in child.parent.previous_siblings:
                        #if not sib == '\n':
                        psibs.append(sib)
                        numsib = numsib+1
                        if(numsib > maxsibs):
                               # print "overflow p"
                                #print len(psibs)
                                #print psibs
                                #raw_input("next")
                                break
                if len(psibs) == 0:
                        #go to parents parent 
                        cp = child.parent 
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.previous_siblings:
                                        psibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                                #psibs = extract_prev_siblings(cp,maxsibs)
                        #if len(psibs) == 0:
                         #       global pzerocount
                         #       pzerocount = pzerocount + 1
        #                print "0 siblings" 

                return psibs 
        def extract_next_siblings(self,child,maxsibs=5):
                numsib = 0
                nsibs=[]
                for sib in child.parent.next_siblings:
         #               if not sib == '\n':
                        nsibs.append(sib)
                        numsib = numsib+1 
                        if(numsib>maxsibs):
                              #  print "overflow n"
                                break
                if len(nsibs) == 0:
                        cp = child.parent
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.next_siblings:
                                        nsibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                        #if len(nsibs) == 0:
                         #       global nzerocount
                         #       nzerocount = nzerocount + 1
               #         print "0 n siblings"
                return nsibs


        def depth_first_traversal(self,soup_body,leafnodes):
                #mark that bit as visited 
                #then go ahead and visit all its unvisted childre
                try:
                        isvisted = getattr(soup_body,"visited",False)
                        if not isvisted:
                                soup_body.visited = True
                                haschildren = getattr(soup_body,"children",False)
                                if not haschildren:
                                    if not soup_body.isspace():
                                       # print soup_body 
                                        leafnodes.append(soup_body)

                                if haschildren:
                                        for soup_child in soup_body.children:
                                                self.depth_first_traversal(soup_child,leafnodes)
                except:
                    pass
                    # print "error"

        def get_parent_attr(self,node):
            attrs={}
            try:
                attrs = getattr(node.parent,"attrs",{})
                while not attrs:
                    node = node.parent
                    attrs = getattr(node.parent,"attrs",{})
            except:
                pass
            return attrs


        def extract_candidates(self,soup):
                all_nodes_labels = []
                endpoint_candidates = []
                y=[]
                html_text = soup.findAll(text=True)
                html_text = ' \n '.join(html_text)
                tag_name = ''
                attrs_main = {}
		possible_tags = ['p']
                leafnodes_l = []
        
                self.depth_first_traversal(soup,leafnodes_l)
                if leafnodes_l is not None:
                #for each leaf node 
                #to account for ... if that word is by itself and it says optional or required 
                #it is NOT what we want. 
                        for leaf in leafnodes_l:
                                name = getattr(leaf,"name",None)
                                attrs = getattr(leaf,"attrs",{})
                                if name is None:
                                    #then we want the name of the parent 
                                        if getattr(leaf,"parent",None):
                                                tag_name = leaf.parent.name
                                        else:
                                                tag_name = ''
                                if not attrs:
                                        attrs = self.get_parent_attr(leaf)
                                toappend = False
                                #print "dont append yet"
                                #so lets go ahead and check whether this node is just optional or required 
                                # opt_req = re.compile('\[*required\]*|\[*optional\]*')
                                # has_opt_req = opt_req.match(leaf)

                                #lets extract useful information from these guys now 
                                #print leaf
                                #raw_input("next")
                                #try:
                                 #       cp = attrs['class']
                                 #       if cp[0] == 'param':
                                 #               print "here"
                                                
                                 #               ipdb.set_trace()
                                #except:
                                #        pass

                                #if has_opt_req:
                                #        print "has the words req"
                                #        ipdb.set_trace()
                                        
                                if tag_name in possible_tags:
                                        try:
                                                if self.label_to_find in attrs['class']:
                                                        y.append(1)
                                                else:
                                                        y.append(0)
                                        except:
                                                y.append(0)
                                        toappend = True
                                 #       print "append"
                                # else:
                                #         xleaf = leaf.strip()
                                #         m = re.search('^[A-z,\[\],_]+$',xleaf)
                                        
                                #         if m:
                                #                 try:
                                #                         if self.label_to_find in attrs['class'] or 'param' in attrs['class']:
                                #                                 y.append(1)
                                #                         else:
                                #                                 y.append(0)
                                #                 except:
                                #                         y.append(0)
                                #                 toappend = True
                                #   #      print "append"
                                if toappend:
                                        endpoint_candidates.append({
                                                        'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)
                                        })
                                all_nodes_labels.append({'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)})

                else:
                    pass
                        # print "List is not iterable"
                if len(y) == len(endpoint_candidates):
                    pass
                        # print "all good so far"


                return (all_nodes_labels,y,endpoint_candidates)



def extract_description_candidates(soup):
	desc_candidates = []
	tag_name = ''
	attrs_main = {}
	for child in soup.recursiveChildGenerator():
		name = getattr(child, "name", None)
		attrs = getattr(child,"attrs",{})
		classes = []	
		if name is None and not child.isspace():
			if tag_name == "p":
				desc_candidates.append(child)
		tag_name = name

	return desc_candidates

def features_extraction(desc_candidates):
	gold_words = ['represents', 'request', 'api', 'post', 'get', 'sends', 'resource', 'call', 'return', 'retrieve']
	features_m = []	
	for dc in desc_candidates:
		f = {}
		previous_siblings = dc['previous_siblings']
		next_siblings = dc['next_siblings']
		text = dc['text']
		tag = dc['tag']

		text_s=text.split(' ')
		for ts in text_s:
			if ts.lower() in gold_words:
				f['contains_gold_word_%s'%ts]=1
		for p in previous_siblings:			
			if type(p).__name__ == 'Tag':
				if getattr(p,"name",None).find('h')>=0:
					f['contains_header_tag_in_siblings'] = 1
			p = p.string
			if p is None:
				p=''
			m = re.search('.*\/.+\/.*', p)
			if m:
				endpoint_cand = m.group()
				f['endpoint_found_in_siblings'] = 1
				endpoint_cand = endpoint_cand.split('/')
				for e in endpoint_cand:
					e = e.replace('_',' ')
					e = re.sub('s$', '', re.sub('ies$', '', e))
					c = text.lower().count(e.lower())
					if c<0:
						f['endpoint_word_occurence_less_than_0']=1
					elif c>0 and c<2:
						f['endpoint_word_occurence_between_0_and_2']=1
					elif c>2 and c<5:
						f['endpoint_word_occurence_between_2_and_5']=1
					elif c>5 and c<10:
						f['endpoint_word_occurence_between_5_and_10']=1
					elif c>10 and c<15:
						f['endpoint_word_occurence_between_10_and_15']=1
					else:
						f['endpoint_word_occurence_greater_than_15']=1
		for n in next_siblings:
			if type(n).__name__ == 'Tag':
				if getattr(n,"name",None).find('h')>=0:
					f['contains_header_tag_in_siblings'] = 1
			n = n.string
			if n is None:
				n=''
			m = re.search('.*\/.+\/.*', n)
			if m:
				endpoint_cand = m.group()
				f['endpoint_found_in_siblings'] = 1
				endpoint_cand = endpoint_cand.split('/')
				for e in endpoint_cand:
					e = e.replace('_',' ')
					e = re.sub('s$', '', re.sub('ies$', '', e))
					c = text.lower().count(e.lower())
					if c<0:
						f['endpoint_word_occurence_less_than_0']=1
					elif c>0 and c<2:
						f['endpoint_word_occurence_between_0_and_2']=1
					elif c>2 and c<5:
						f['endpoint_word_occurence_between_2_and_5']=1
					elif c>5 and c<10:
						f['endpoint_word_occurence_between_5_and_10']=1
					elif c>10 and c<15:
						f['endpoint_word_occurence_between_10_and_15']=1
					else:
						f['endpoint_word_occurence_greater_than_15']=1
		features_m.append(f)
	return features_m


def get_most_occuring_words(sentences):
	counts = Counter()
	for ii in sentences:
		counts.update(word.strip('.,?!"\'').lower() for word in ii.split())
