import re
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib

def extract_candidates(soup):
	endpoint_candidates = []

	html_text = soup.findAll(text=True)
	html_text = ' \n '.join(html_text)
	tag_name = ''
	attrs_main = {}
	for child in soup.recursiveChildGenerator():
		name = getattr(child, "name", None)
		attrs = getattr(child,"attrs",{})
		classes = []	
		if name is None and not child.isspace():
			if tag_name == "code" or tag_name == "pre":
				try:
					if 'api_endpoint' in attrs_main['class']:
						attrs_main['class']=[]
				except:
					pass
				endpoint_candidates.append({
					'attrs':attrs_main,
					'tag':tag_name,
					'text':child,
					'parent':child.parent,
					'html_text':html_text,
					'previous_sibling':child.parent.previous_sibling,
					'next_sibling':child.parent.next_sibling
					})
			else:			
				m = re.search('.*\/.+\/.*', child)
				if m:
					try:
						if 'api_endpoint' in attrs_main['class']:
							attrs_main['class']=[]
					except:
						pass
					endpoint_candidates.append({
						'attrs':attrs_main,
						'tag':tag_name,
						'text':m.group(),
						'parent':child.parent,
						'html_text':html_text,
						'previous_sibling':child.parent.previous_sibling,
						'next_sibling':child.parent.next_sibling
						})
		tag_name = name
		if attrs:
			attrs_main = attrs
	return endpoint_candidates



def features_extraction(endpoint_candidates):
	features = []
	largest_occurence = 0
	for e in endpoint_candidates:
		f={}
		tag = e['tag']
		parent = e['parent']
		text = e['text']
		html_text = e['html_text']
		previous_sibling = e['previous_siblings']
		next_sibling = e['next_siblings']
		f['has_tag_%s'%tag] = 1
		try:
			classes = e['attrs']['class']
			for c in classes:
				if c=='api_endpoint':
					pass
				else:
					pass
					# f['has_class_%s'%c] = 1
		except:
			pass
		if len(text)<10:
			f['length_smaller_than_10']=1
		elif len(text)>10 and len(text)<20:
			f['length_in_between_10_and_20'] = 1
		elif len(text)>20 and len(text)<30:
			f['length_in_between_20_and_30'] = 1
		elif len(text)>30 and len(text)<40:
			f['length_in_between_30_and_40'] = 1
		elif len(text)>40 and len(text)<50:
			f['length_in_between_40_and_50'] = 1
		elif len(text)>50 :
			f['length_greater_than_50'] = 1

		number_of_slash = text.count('/')
		if number_of_slash<3:
			f['slashes_less_than_3'] = 1
		elif number_of_slash>3 and number_of_slash<6:
			f['slashes_in_between_3_and_6'] = 1
		elif number_of_slash>6 and number_of_slash<9:
			f['slashes_in_between_6_and_9'] = 1
		elif number_of_slash>9 and number_of_slash<15:
			f['slashes_in_between_9_and_15'] = 1
		elif number_of_slash>15:
			f['slashes_greater_than_15'] = 1

		bracket_text = ''
		occurences = 0
		if text.find('{')>=0 or text.find('['):
			f['contains_parenthesis'] = 1
			f['curly_parenthesis_count_%s'%text.count('{')] = 1
			f['square_parenthesis_count_%s'%text.count('[')] = 1	
			m = re.search('{(.*)}', text)
			if m:
				bracket_text = m.group(1)
			if bracket_text:
				occurences = html_text.lower().count(bracket_text.lower())
		if occurences>largest_occurence:
			largest_occurence = occurences
		if occurences < 10:
			f['occurences_less_than_10']=1
		elif occurences>10 and occurences<20:
			f['occurences_in_between_10_and_20'] = 1
		elif occurences>20 and occurences<30:
			f['occurences_in_between_20_and_30'] = 1
		elif occurences>30 and occurences<40:
			f['occurences_in_between_30_and_40'] = 1
		elif occurences>40 and occurences<50:
			f['occurences_in_between_40_and_50'] = 1
		elif occurences>50:
			f['occurences_greater_than_50'] = 1
		if parent.text.lower().find('get') or parent.text.lower().find('post')>=0:
			f['contains_get_post'] = 1


		slash_seperated_text = text.split('/')
		# print slash_seperated_text
		if len(slash_seperated_text)>=3:
			slash_seperated_text = slash_seperated_text[-2:]
		for s in slash_seperated_text:
			s = s.replace('_',' ')
			s = re.sub('s$', '', re.sub('ies$', '', s))
			c = html_text.lower().count(s.lower())
			if c<=0:
				f['endpoint_word_occurence_equal_to_zero']=1
			elif c>0 and c<5:
				f['endpoint_word_occurence_less_than_5']=1
			elif c>5 and c<10:
				f['endpoint_word_occurence_between_5_and_10']=1
			elif c>10 and c<15:
				f['endpoint_word_occurence_between_10_and_15']=1
			elif c>15 and c<20:
				f['endpoint_word_occurence_between_15_and_20']=1
			elif c>20 and c<25:
				f['endpoint_word_occurence_between_20_and_25']=1
			elif c>25 and c<30:
				f['endpoint_word_occurence_between_25_and_30']=1
			elif c>30 and c<35:
				f['endpoint_word_occurence_between_30_and_35']=1
			elif c>35 and c<40:
				f['endpoint_word_occurence_between_35_and_40']=1
			elif c>40 and c<50:
				f['endpoint_word_occurence_between_40_and_50']=1
			elif c>50:
				f['endpoint_word_occurence_greater_than_50']=1


		if type(previous_sibling).__name__ == 'Tag':			
			if previous_sibling.name.find('h')>=0:
				f['previous_sibling_contains_header_tag'] = 1
			if len(previous_sibling.text.split(' '))<5:
				f['previous_sibling_text_less_than_five'] = 1
		elif type(previous_sibling).__name__ == 'NavigableString':
			if len(previous_sibling.split(' '))<5 and not previous_sibling.strip()=='':
				f['previous_sibling_text_less_than_five'] = 1

		features.append(f)


	count_candidates = []
	for e in endpoint_candidates:
		previous_sibling = e['previous_siblings']
		next_sibling = e['next_siblings']
		previous_sibling_text = ''
		next_sibling_text = ''	
		for p in previous_sibling:
			if type(p).__name__ == 'Tag':
				previous_sibling_text =  previous_sibling_text + '\n' + p.text
			elif type(p).__name__ == 'NoneType':
				pass
		for n in next_sibling:
			if type(n).__name__ == 'Tag':
				next_sibling_text = next_sibling_text + '\n' + n.text
			elif type(n).__name__ == 'NoneType':
				pass

		count_candidates.append(previous_sibling_text + '\n' + next_sibling_text)		
	#count_features = text_features_extraction(count_candidates, v_count, kbest_count)

	return features , count_candidates

# def text_features_extraction(text, v_count, kbest_count):	
# 	return v_count.fit_transform(text)


class endpoint_funcs_class:
        label_to_find = 'api_endpoint'                    	
        def extract_prev_siblings(self,child,maxsibs=5):
                numsib = 0
                psibs=[]
                for sib in child.parent.previous_siblings:
                        psibs.append(sib)
                        numsib = numsib+1
                        if(numsib > maxsibs):
                                break
                if len(psibs) == 0:
                        cp = child.parent 
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.previous_siblings:
                                        psibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                return psibs 
        def extract_next_siblings(self,child,maxsibs=5):
                numsib = 0
                nsibs=[]
                for sib in child.parent.next_siblings:
                        nsibs.append(sib)
                        numsib = numsib+1 
                        if(numsib>maxsibs):
                                break
                if len(nsibs) == 0:
                        cp = child.parent
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.next_siblings:
                                        nsibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                return nsibs

        def depth_first_traversal(self,soup_body,leafnodes):
                try:
                        isvisted = getattr(soup_body,"visited",False)
                        if not isvisted:
                                soup_body.visited = True
                                haschildren = getattr(soup_body,"children",False)
                                if not haschildren:
                                    if not soup_body.isspace():
                                       # print soup_body 
                                        leafnodes.append(soup_body)

                                if haschildren:
                                        for soup_child in soup_body.children:
                                                self.depth_first_traversal(soup_child,leafnodes)
                except:
                    pass

        def get_parent_attr(self,node):
            attrs={}
            try:
                attrs = getattr(node.parent,"attrs",{})
                while not attrs:
                    node = node.parent
                    attrs = getattr(node.parent,"attrs",{})
            except:
                pass
            return attrs


        def extract_candidates(self,soup):
                all_nodes_labels = []
                endpoint_candidates = []
                y=[]
                html_text = soup.findAll(text=True)
                html_text = ' \n '.join(html_text)
                tag_name = ''
                attrs_main = {}
                possible_tags = ['pre','code']
                leafnodes_l = []
        
                self.depth_first_traversal(soup,leafnodes_l)
                if leafnodes_l is not None:
                        for leaf in leafnodes_l:
                                name = getattr(leaf,"name",None)
                                attrs = getattr(leaf,"attrs",{})
                                if name is None:
                                    #then we want the name of the parent 
                                        if getattr(leaf,"parent",None):
                                                tag_name = leaf.parent.name
                                        else:
                                                tag_name = ''
                                if not attrs:
                                        attrs = self.get_parent_attr(leaf)
                                toappend = False
                                if tag_name in possible_tags:
                                        try:
                                                if self.label_to_find in attrs['class']:
                                                        y.append(1)
                                                else:
                                                        y.append(0)
                                        except:
                                                y.append(0)
                                        toappend = True
                                else:
                                	m = re.search('.*\/.+\/.*', leaf)
                                	if m:
										try:
											if self.label_to_find in attrs['class']:
												y.append(1)
											else:
												y.append(0)
										except:
											y.append(0)
										toappend = True
                            
                                if toappend:
                                        endpoint_candidates.append({
                                                        'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)
                                        })
                                all_nodes_labels.append({'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)})
                else:
                    pass
                if len(y) == len(endpoint_candidates):
                    pass
                return (all_nodes_labels,y,endpoint_candidates)