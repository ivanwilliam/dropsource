*ReadMe for Dropsource Project*

Please make sure you have python 2.7 and pip installed and run these commands:

`sudo pip install numpy`
`sudo pip install scipy`
`sudo pip install scikit-learn` (numpy and scipy are scikit dependencies)
`sudo pip install BeautifulSoup4`
