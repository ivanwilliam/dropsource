import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
# adding the ipython debugger 
from IPython.core.debugger import Tracer
import ipdb

#something to store leafnodes in, needs to be set to [] for each page
leafnodes = []

def soup_dfs(soup_body):
        #mark that bit as visited 
        #then go ahead and visit all its unvisted childre 
        isvisted = getattr(soup_body,"visited",False)
        if not isvisted:
                soup_body.visited = True
                haschildren = getattr(soup_body,"children",False)
                if not haschildren:
                    if not soup_body.isspace():
                        print soup_body 
                        leafnodes.append(soup_body)

                if haschildren:
                        for soup_child in soup_body.children:
                                soup_dfs(soup_child)

def get_parent_attr(node):
    attrs={}
    try:
        attrs = getattr(node.parent,"attrs",{})
        while not attrs:
            node = node.parent
            attrs = getattr(node.parent,"attrs",{})
    except:
        pass
    return attrs


def extract_candidates(leafnodes_list):
    #for each leaf node 
    for leaf in leafnodes_list:
        name = getattr(leaf,"name",None)
        attrs = getattr(leaf,"attrs",{})
        if name is None:
            #then we want the name of the parent 
            if getattr(leaf,"parent",None):
                tag_name = leaf.parent.name
            else:
                tag_name = ''
            if not attrs:
                attrs = get_parent_attr(leaf)
            print tag_name 
            print attrs
                
            '''if tag_name == "code" or tag_name =="pre":
                	try:
                            if 'api_endpoint' in attrs['class']:
                                attrs_m['class']=[]
                                y.append(1)
                            else:
                                y.append(0)
                            except:
                                y.append(0)
                                #so add anything with a code or pre tag as a candidate
				endpoint_candidates.append({
					'attrs':attrs_main,
					'tag':tag_name,
					'text':child,
					'parent':child.parent,
					'html_text':html_text,
					'previous_sibling':child.parent.previous_sibling,
					'next_sibling':child.parent.next_sibling, #fatma edit adding the prev and next 3 siblings to this 
                                        'previous_siblings':extract_prev_siblings(child,num_max_prev_next_siblings),
                                        'next_siblings': extract_next_siblings(child,num_max_prev_next_siblings)

					})
                                
                                
                                #otherwise search for a url
                            else:
                                m = re.search('.*\/.+\/.*', child)
                                #if you find it 
				if m:
                                        #label here again 
					try:
						if 'api_endpoint' in attrs_main['class']:
							attrs_main['class']=[]
							y.append(1)
						else:
							y.append(0)
					except:
						y.append(0)
                                        #add it to the candidates 
					endpoint_candidates.append({
						'attrs':attrs_main,
						'tag':tag_name,
						'text':m.group(),
						'parent':child.parent,
						'html_text':html_text,
						'previous_sibling':child.parent.previous_sibling,
						'next_sibling':child.parent.next_sibling, #fatma edit getting prev and next 3 siblings 
                                                'previous_siblings':extract_prev_siblings(child,num_max_prev_next_siblings),
                                                'next_siblings':extract_next_siblings(child,num_max_prev_next_siblings)
						})'''
                
    

#setting globals / fixed values 
html_folder = 'Labelled html files/'
html_files = glob.glob('%s*.html' % html_folder)
html_files = sorted(html_files)
# html_files = [html_files[-1]]
soup_main = []
html_pages = []
api_heading = []
api_endpoint = []
api_description = []
all_code_tags = []
endpoint_candidates = []
y = []

#fatma edit adding number of previous and next siblings 
num_max_prev_next_siblings = 3

#doing this for just one file 
f = html_files[0]

page = urllib.urlopen(f).read() #read the page
soup = BeautifulSoup(page, "lxml")
#finding labels 
heading = soup.findAll(True, {'class': 'api_heading'})
endpoint = soup.findAll(True, {'class': 'api_endpoint'})
container = soup.findAll(True, {'class': 'api_container'})
#add page to the real soup 
soup_main.append(soup)
#find and break html text for this page
html_text = soup.findAll(text=True)
html_text = ' \n '.join(html_text)

#perform dft  
soup_dfs(soup)
#just print names and attributes 
extract_candidates(leafnodes)
