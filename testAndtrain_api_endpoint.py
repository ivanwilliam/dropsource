import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
from endpoint_funcs import *
from utility_funcs import *
from scipy.sparse import coo_matrix, hstack
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import FeatureUnion


html_folder1 = 'Labelled html files/'
html_files1 = glob.glob('%s*.html' % html_folder1)
html_files1 = sorted(html_files1)
html_folder2 = 'html_files/'
html_files2 = glob.glob('%s*.html' % html_folder2)
html_files2 = sorted(html_files2)
html_folder3 = 'html_files_2/'
html_files3 = glob.glob('%s*.html' % html_folder3)
html_files3 = sorted(html_files3)

html_files = html_files1 + html_files2 + html_files3
# html_files = [html_files[-1]]
soup_main = []
html_pages = []
api_heading = []
api_endpoint = []
api_description = []
all_code_tags = []
endpoint_candidates = []
y = []



def get_all_text(soup, data = []):
	if type(soup).__name__ == 'NavigableString':
		data.append(type(soup).__name__ +"  --> " + soup)
		return
	else:
		try:
			if 'children' in dir(soup):
				for ch in soup.children:
					get_all_text(ch, data)
		except Exception as ex:
			print(ex)
			global y
			y = ch
	return




ef = endpoint_funcs_class()
endpoint_candidates_new = []
y_new = []

print 'Extracting Candidates . . .'
for f in html_files: 
	page = urllib.urlopen(f).read()
	soup = BeautifulSoup(page, "lxml")
	heading = soup.findAll(True, {'class': 'api_heading'})
	endpoint = soup.findAll(True, {'class': 'api_endpoint'})
	container = soup.findAll(True, {'class': 'api_container'})
	soup_main.append(soup)
	html_text = soup.findAll(text=True)
	html_text = ' \n '.join(html_text)

	(all_nodes_labels,y_n,ec) = ef.extract_candidates(soup)
	endpoint_candidates_new = endpoint_candidates_new + ec
	y_new = y_new + y_n



	# tag_name = ''
	# attrs_main = {}
	# for child in soup.recursiveChildGenerator():
	# 	name = getattr(child, "name", None)
	# 	attrs = getattr(child,"attrs",{})
	# 	classes = []	
	# 	if name is None and not child.isspace():
	# 		if tag_name == "code" or tag_name == "pre":
	# 			try:
	# 				if 'api_endpoint' in attrs_main['class']:
	# 					attrs_main['class']=[]
	# 					y.append(1)
	# 				else:
	# 					y.append(0)
	# 			except:
	# 				y.append(0)
	# 			endpoint_candidates.append({
	# 				'attrs':attrs_main,
	# 				'tag':tag_name,
	# 				'text':child,
	# 				'parent':child.parent,
	# 				'html_text':html_text,
	# 				'previous_sibling':child.parent.previous_sibling,
	# 				'next_sibling':child.parent.next_sibling
	# 				})
	# 		else:			
	# 			m = re.search('.*\/.+\/.*', child)
	# 			if m:
	# 				try:
	# 					if 'api_endpoint' in attrs_main['class']:
	# 						attrs_main['class']=[]
	# 						y.append(1)
	# 					else:
	# 						y.append(0)
	# 				except:
	# 					y.append(0)
	# 				endpoint_candidates.append({
	# 					'attrs':attrs_main,
	# 					'tag':tag_name,
	# 					'text':m.group(),
	# 					'parent':child.parent,
	# 					'html_text':html_text,
	# 					'previous_sibling':child.parent.previous_sibling,
	# 					'next_sibling':child.parent.next_sibling
	# 					})
	# 	tag_name = name
	# 	if attrs:
	# 		attrs_main = attrs

	for e in endpoint:
		api_endpoint.append(e.getText())


print 'Candidates Extracted'

clf = RandomForestClassifier(verbose=0)
v = DictVectorizer(sparse=False)
v_count = CountVectorizer(ngram_range=(1,3))
kbest_count = SelectKBest(chi2, k=50)

print 'Extracting Features . . .'
candidates_features, count_features_text = features_extraction(endpoint_candidates_new)
print 'Features Extracted'

count_features = v_count.fit_transform(count_features_text)
count_features = kbest_count.fit_transform(count_features, y_new)
X = v.fit_transform(candidates_features)
combined_features = hstack((X, count_features),format='csr')

# asd

clf = clf.fit(combined_features, y_new)
save_classifier_data([ clf, v, v_count , kbest_count ], 'models/api_endpoint_clf(22-3-16)_new1.gzip')