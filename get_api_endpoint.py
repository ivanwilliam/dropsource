import sys
import urllib
from utility_funcs import *
from endpoint_funcs import *
from bs4 import BeautifulSoup
from urllib2 import urlopen
from scipy.sparse import coo_matrix, hstack


def load_classifier_api(path):
	clf_data= load_classifier_data(path)
	clf = clf_data[0]
	v = clf_data[1]
	v_count = clf_data[2]
	kbest_count = clf_data[3]
	return clf, v, v_count, kbest_count

def get_highest_prob(prob):
	prob_candidates = []
	for p in prob:
		prob_candidates.append(p[1])
	index = prob_candidates.index(max(prob_candidates))	
	return index



if __name__ == '__main__':

	# page = sys.argv[1]

	ef = endpoint_funcs_class()

	page = urllib.urlopen('Labelled html files/test.html').read()

	page = page.replace('<wbr>','')
	page = page.replace('</wbr>','')

	print 'Loading Classifiers'
	clf, v, v_count, kbest_count = load_classifier_api('models/api_endpoint_clf(22-3-16)_new1.gzip')
	soup = BeautifulSoup(page, "lxml")
	print 'Extracting Candidates'
	# endpoint_candidates = extract_candidates(soup)
	(all_nodes_labels,y_n,endpoint_candidates) = ef.extract_candidates(soup)
	print 'Extracting Features . . .'
	candidates_features, count_features_text = features_extraction(endpoint_candidates)
	count_features = v_count.transform(count_features_text)
	count_features = kbest_count.transform(count_features)
	X = v.transform(candidates_features)
	combined_features = hstack((X, count_features),format='csr')
	prob = clf.predict_proba(combined_features)
	pred = clf.predict(combined_features)

	highest_prob = get_highest_prob(prob)
	if prob[highest_prob][1]==0:
		print 'No Highest Prob candidate found'
		# return 0
	else:
		print endpoint_candidates[highest_prob]['text']
		# return endpoint_candidates[highest_prob]['text']
