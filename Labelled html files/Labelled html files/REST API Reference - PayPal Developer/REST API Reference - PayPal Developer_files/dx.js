requirejs.config({
    paths: {
        jquery: '/components/jquery/dist/jquery.min'
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        }
    }
});

define(['jquery'], function ($) {
    'use strict';

    var $window = $(window),
        $body = $('body'),
        $main = $body.find('.dx-main'),
        $mainNav = $main.find('.dx-main-nav'),
        hasMatchMedia = 'matchMedia' in window;

    return {
        /**
         * For mobile devices, stuff the left nav into a short, full-width scroll window that can be expanded vertically.
         */
        mobilizeLeftNav: function () {
            var breakpoint = '59rem';

            // Run the first time thru.
            if (isMobileWidth()) {
                centerMenuItem();
                buildToggler();
                handleMobileNavEvents();
            }

            // Re-run scripts on page resize.
            $window
                .on('resize', function (ev) {
                    centerMenuItem();
                    buildToggler();
                    handleMobileNavEvents();
                });

            /**
             * Determine if current browser window is predefined mobile width.
             * @return {Boolean} Is mobile width?
             */
            function isMobileWidth() {
                // First check if matchMedia is available, if not then this probably isn't modern mobile browser.
                // Then check breakpoint, returning a boolean.
                return (hasMatchMedia && window.matchMedia('screen and (min-width:' + breakpoint + ')').matches) ? false : true;
            }

            /**
             * Create the mobile navigation toggler when needed.
             */
            function buildToggler() {
                if (isMobileWidth()) {
                    var $navExpander = $mainNav.find('.dx-main-nav-expander');

                    if (! $navExpander[0]) {
                        var toggleText = $mainNav.data('toggle-text') || 'Toggle navigation';

                        $mainNav.prepend('<div class="dx-main-nav-expander"><em class="sr-only">' + toggleText + '</em></div>');
                    }
                }
            }

            /**
             * Vertically center the active navigation link in a given window.
             */
            function centerMenuItem() {
                // get the active nav element
                var $active = $mainNav.find('.dx-nav-active');

                // Make sure we have an active element to work with.
                if (! $active[0]) {
                    return;
                }

                var $element = $active.find('a').first();
                var $parent = $mainNav.find('> .dx-nav');

                // Vertically center the active element in the given window.
                $parent
                    .animate({
                            scrollTop: $parent.scrollTop() + $element.offset().top - $parent.offset().top - ($parent.outerHeight() / 2) + ($element.outerHeight() / 2)
                        },
                        100,
                        'swing'
                    );
            }

            /**
             * Bind mobile nav events when within mobile dimensions.
             * Unbind events when larger than mobile size.
             */
            function handleMobileNavEvents() {
                if (isMobileWidth()) {
                    // Expand the scroll window vertically to see more nav links.
                    $body
                        // Expand when clicking anywhere in nav window while it's closed,
                        // OR when clicking on expander element.
                        .on('click.mobileNav', '.dx-main-nav:not(".dx-main-nav-expanded"), .dx-main-nav-expander', function (ev) {
                            ev.preventDefault();
                            // Allow only 1 triggered event from .dx-main-nav-expander.
                            ev.stopPropagation();

                            $mainNav
                                .toggleClass('dx-main-nav-expanded')
                                .delay(250)
                                    .queue(function () {
                                        // Re-center menu.
                                        centerMenuItem();
                                        $(this).dequeue();
                                    });
                        });

                } else {
                    $body.off('click.mobileNav');
                }
            }
        },

        /**
         * Trigger show/hide of the left hand mobile navigation tray.
         */
        toggleMobileTray: function () {
            $body
                .find('.dx-mobile-tray-trigger')
                    .on('click', function (ev) {
                        $body
                            .toggleClass('dx-mobile-tray-open');
                    });
        },

        /**
         * Highlight the current page in the left-hand navigation.
         * @return {[type]} [description]
         */
        emphasizeActiveLeftNav: function () {
            $mainNav
                .find('.dx-nav-group li:has(a[href="' + location.pathname + '"])')
                    .addClass('dx-nav-active-parent')
                    .last()
                        .removeClass(function (idx, cls) {
                            // if not the top-level list item, then remove the 'dx-nav-active-parent' class
                            if (! $(this).find('.dx-nav-head, .dx-nav')[0]) {
                                return 'dx-nav-active-parent';
                            }
                        })
                        .addClass('dx-nav-active');
        },

        /**
         * PAYPAL-DEVELOPER-DOCS VERSION.
         * Highlight the current page in the left-hand navigation.
         */
        showActiveNav: function (targetSelector) {
            // The default 'a[href="./"]' selector is paypal-developer-docs specific.
            targetSelector = targetSelector || '.dx-nav-group li:has(a[href="./"])';

            $mainNav
                .find(targetSelector)
                    .addClass('dx-nav-active-parent')
                    // If not the top-level list item,
                    // then remove the 'dx-nav-active-parent' class
                    // and add 'dx-nav-active' class.
                    .last()
                        .removeClass(function (idx, cls) {
                            if (! $(this).find('.dx-nav-head, .dx-nav')[0]) {
                                return 'dx-nav-active-parent';
                            }
                        })
                        .addClass('dx-nav-active')
                        ;
        },

        /**
         * Display the active hash link in left-hand navigation.
         * @param  {Object} ev Hashchange event object.
         */
        showActiveNavHash: function (ev) {
            var hash = window.location.hash;

            if (ev) {
                // Use the event's hash.
                hash = ev.target.location.hash;
            } else {
                // Unless we're entering with an event, add hashchange listener.
                $window.on('hashchange', this.showActiveNavHash);
            }

            if (hash) {
                $mainNav
                    .find('li')
                        .removeClass('dx-nav-active-hash')
                        .has('a[href$="' + hash + '"]')
                            .last()
                            .addClass('dx-nav-active-hash');
            }
        },

        /**
         * Treat external links differently: add class, title, and target attributes.
         */
        emphasizeExternalLinks: function () {
            // Find links in main and foot sections with an href that starts with `http`.
            $main
                .add('.dx-foot')
                .find('a[href^=http]')
                    // Filter out the links from `developer.paypal.com`.
                    .filter(function () {
                        return !/^https:\/\/developer.paypal.com/.test(this.href);
                    })
                        .addClass('dx-external-link')
                        .attr('title', 'external link')
                        .attr('target', '_blank');
        },

        /**
         * Add GitHub-style linkable headers.
         */
        addAnchorLinks: function () {
            $body
                .filter('.dx-docs, .dx-apiref')
                    .find('h1[id], h2[id], h3[id]')
                        .addClass('dx-anchor')
                        .prepend(function () {
                            return '<a href="#' + this.id + '" aria-hidden=true></a>';
                        });
        },

        /**
         * Reveal scroll-to-top link when user scrolls below the page fold.
         */
        scrollToTop: function () {
            var body = document.documentElement || document.body;
            var windowHeight = window.outerHeight;

            // Only reveal scrollToTop if we have a tall page.
            if (body.scrollHeight > windowHeight) {
                // Reveal scrollToTop if we start below page fold.
                if (getScrollTop() > windowHeight) {
                    revealScrollToTop();
                } else {
                    $window
                        .on('scroll', function (ev) {
                            // Only add scrollToTop if we drop below page fold.
                            if (getScrollTop() > windowHeight) {
                                // Remove onscroll event handler since we don't need it anymore.
                                $window.off('scroll');

                                revealScrollToTop();
                            }
                        });
                }
            }

            /**
             * Get the topmost coordinate of the scrollbar.
             * @return {Number} Topmost coordinate, in pixels.
             */
            function getScrollTop() {
                return document.documentElement.scrollTop || document.body.scrollTop;
            }

            /**
             * Show the link and watch for clicks.
             */
            function revealScrollToTop() {
                // Reveal scroll-to-top link.
                $body
                    .find('.dx-scrolltop')
                        .addClass('dx-unhide');

                // Jump to top if link clicked.
                $body
                    .on('click', '.dx-scrolltop a', function (ev) {
                        // Don't follow link...
                        ev.preventDefault();
                        // ...just jump to the top.
                        window.scrollTo(0, 0);
                    });
            }
        },

        /**
         * Load analytics packages and their tracking scripts.
         */
        loadAnalytics: function () {
            var self = this;
            var hostname = window.location.hostname;

            // When not in development mode...
            if (hostname.indexOf('localhost') === -1) {
                loadGoogleAnalytics();
                initGaTracking();
                loadPayPalAnalytics();
                initPpTracking();
            }


            function loadGoogleAnalytics() {
                var gaId = (hostname === 'developer.paypal.com') ? 'UA-37159521-1' : 'UA-37419067-1';

                // Load GA onto page.
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', gaId, 'auto');
                ga('send', 'pageview');
            }

            function initGaTracking() {
                // Track in-page hash changes.
                $window
                    .on('hashchange', function (ev) {
                        var newHash = ev.target.location && ev.target.location.hash;

                        // Record if hash isn't empty.
                        if (newHash && newHash.length > 1) {
                            self.trackEvent({
                                cat: 'inPageAnchor',
                                act: 'click',
                                lbl: newHash
                            });
                        }
                    });

                // Track clicks on external links.
                $body
                    .on('click', 'a[href^="http"]', function (ev) {
                        var href = ev.target.href;
                        var target = ev.target.target;

                        if (href && href.indexOf(hostname) === -1) {
                            ev.preventDefault();

                            self.trackEvent({
                                cat: 'externalLink',
                                act: 'click',
                                lbl: href
                            });

                            return target ? window.open(href, target) : window.location.assign(href);
                        }
                    });

                // Track search submissions.
                $body
                    .on('submit', '.dx-form-search', function (ev) {
                        var inputValue = $(ev.target).find('.dx-search-input').val();

                        self.trackEvent({
                            cat: 'siteSearch',
                            act: 'submit',
                            lbl: inputValue
                        });
                    });

                // Track clicks on search results.
                $body
                    .on('click', '.dx-search-result-link', function (ev) {
                        self.trackEvent({
                            cat: 'siteSearch',
                            act: 'clickResult',
                            lbl: ev.target.href
                        });
                    });
            }

            function loadPayPalAnalytics() {
                var pas = document.createElement('script');

                pas.id = 'PayPalAnalytics';
                pas.src = 'https://www.paypalobjects.com/pa/js/pa.js';

                document.body.appendChild(pas);
            }

            function initPpTracking() {
                (function ($) {
                    var dataString = 'tmpl=' + window.location.pathname;

                    if (typeof window.PAYPAL !== 'undefined' && window.PAYPAL.analytics) {
                        window.PAYPAL.core = window.PAYPAL.core || {};
                        window.PAYPAL.core.pta = window.PAYPAL.analytics.setup({
                            data: dataString,
                            url: 'https://tracking.qa.paypal.com/webapps/tracking/ts'
                        });
                    }
                }());
            }
        },

        /**
         * Send event tracking beacon to analytics packages.
         * @param  {Object} eventObject
         *             {String} cat Category
         *             {String} act Action
         *             {String} lbl Label
         *             {String} val Value
         */
        trackEvent: function (eventObject) {
            // Make sure we have an event object and that Google Analytics code is loaded.
            if (eventObject && window.ga) {
                // FWIW, Google Analytics doesn't seem to mind undefined args so we'll pass them all.
                window.ga('send', 'event', {
                    'eventCategory': eventObject.cat,
                    'eventAction': eventObject.act,
                    'eventLabel': eventObject.lbl,
                    'eventValue': eventObject.val
                });
            }
        },

        /**
         * Set up code language switcher.
         */
        setupCodeLanguagesSwitcher: function () {
            var self = this;
            var localStorageKey = 'paypal-code-lang';
            var defaultLang = 'bash';

            // Get the language preference if localStorage is an option.
            if (window.localStorage) {
                var lang = window.localStorage.getItem(localStorageKey);

                selectCodeLanguage(lang, true);
            }

            // Handle language button selection
            $body.on('click', '.languageOptions button', handleLanguageButtonClick);

            /**
             * Update the DOM according to given example code lang.
             * @param  {String}  lang   OPTIONAL Default code language.
             * @param  {Boolean} preset OPTIONAL Settings retrieved from localStorage.
             */
            function selectCodeLanguage(lang, preset) {
                var $buttons = $main.find('.languageOptions button');
                var languageOptions = $.map($buttons, function (ev) {
                    return $(ev).attr('data-lang');
                });

                // Use a default if language not found.
                if ($.inArray(lang, languageOptions) < 0) {
                    lang = languageOptions[0] || defaultLang;
                }

                // Mark this button as active, deactivate siblings.
                var $activeButton = $buttons.filter('[data-lang=' + lang + ']');

                if (! $activeButton.length) {
                    $activeButton = $buttons.first();
                }

                $activeButton
                    .addClass('dx-btn-active')
                    .siblings()
                        .removeClass('dx-btn-active');

                $main
                    .find('.switch')
                        .hide()
                        .removeAttr('data-active')
                        .filter('.switch.switch-' + lang)
                            .show()
                            .attr('data-active', 'true');

                // Store the preference if localStorage is an option.
                if (window.localStorage) {
                    window.localStorage.setItem(localStorageKey, lang);
                }

                // Track event whether retrieved from localStorage or clicked.
                self.trackEvent({
                    cat: 'languageSwitch',
                    act: preset ? 'preset' : 'click',
                    lbl: lang
                });
            }

            /**
             * Select a new language based on the button's data-lang attribute
             * @param  {Object} ev Event object.
             */
            function handleLanguageButtonClick(ev) {
                ev.preventDefault();

                var lang = ev.currentTarget.getAttribute('data-lang');

                selectCodeLanguage(lang);
            }
        },

        /**
         * Highlight keywords in targeted search results page.
         */
        markSearchKeywords: function () {
            var searchParam = location.search;

            if (! searchParam) {
                return;
            }

            // Extract keyword from a query string that looks like '?mark=keyword' or '?query=string&mark=keyword'.
            var keywordMatch = searchParam.match(/[?&]mark=([^&]+)/);

            if (keywordMatch) {
                var container = document.getElementsByClassName('dx-content')[0];
                var content = container.innerHTML;
                var pattern = new RegExp('(>[^<.]*)(' + keywordMatch[1] + ')([^<.]*)', 'ig');
                var replaceWith = '$1<mark>$2</mark>$3';
                var marked = content.replace(pattern, replaceWith);

                // Replace old content with newly marked content.
                container.innerHTML = marked;
            }
        },

        /**
         * Run all the scripts!
         */
        runAll: function () {
            this.showActiveNav();
            this.showActiveNavHash();
            this.emphasizeExternalLinks();
            this.mobilizeLeftNav();
            this.toggleMobileTray();
            this.scrollToTop();
            this.markSearchKeywords();
        }
    };
});
