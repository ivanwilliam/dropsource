requirejs.config({
    paths: {
        jquery: '/components/jquery/dist/jquery.min',
        highlight: '/components/highlightjs/highlight.pack.min'
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        }
    }
});

define(['jquery', 'highlight'], function ($) {
    'use strict';

    var $body = $('body'),
        $main = $body.find('.dx-main'),
        $mainNav = $main.find('.dx-main-nav');

    return {
        /**
         * Apply HighlightJS script/style to <pre><code> blocks within the main content.
         */
        highlightCodeBlocks: function () {
            // Find <pre> blocks. Add class for styling.
            var $pre = $main.find('.dx-content pre').addClass('dx-pre-hljs');

            // Find <pre> blocks that are missing <code> tags and wrap with <code> tags.
            $pre.not(':has(> code)').wrapInner('<code/>');

            hljs.configure({
                // Only look for the following languages:
                languages: [
                    'bash',
                    'cs',
                    'java',
                    'javascript',
                    'json',
                    'php',
                    'python',
                    'ruby'
                ]
            });

            // Find `pre > code` blocks and apply highlighterjs script.
            $pre.find('> code')
                .each(function (i, block) {
                    hljs.highlightBlock(block);
                });
        }
    };
});
