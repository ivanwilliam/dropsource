'use strict';

var fadeTime = 500;

requirejs.config({
    paths: {
        dx: '/components/dx/js/dx',
        hljs: '/components/dx/js/dx-highlight',
        jquery: '/components/jquery/dist/jquery.min'
    },

    baseUrl: '/js',

    shim: {
        jquery: {
            exports: 'jQuery'
        }
    }
});


require(['jquery', 'dx', 'hljs'], function ($, dx, hljs) {
    dx.runAll();
    hljs.highlightCodeBlocks();


    var $body = $('body'),
        $headMenu = $body.find('#header').find('.dx-head-menu'),
        $authUsername = $headMenu.find('.dx-auth-username'),
        $authBtnLogin = $headMenu.find('.dx-auth-login'),
        $authBtnLogout = $headMenu.find('.dx-auth-logout'),
        $authBtnSignup = $headMenu.find('.dx-auth-signup');

    // Seed the appropriate URLs into the login and logout buttons.
    function seedButtons (response) {
        var pageUrl = document.location.href;

        // If the pageUrl is a stage (i.e. .qa.paypal.com), then we must strip off the leading 'www'.
        // Regardless, the url must be URI encoded afterwards.
        if (pageUrl.match(/\.qa\.paypal\.com/) && pageUrl.match(/^https\:\/\/www\./)) {
            pageUrl = 'https://' + pageUrl.substring(12);
        }
        pageUrl = encodeURIComponent(pageUrl);

        if (response) {
            var login = response.login,
                logout = response.logout,
                signUp = response.signUp;

            if (login) {
                $authBtnLogin
                    .attr('href', login.url.replace(/PAGEURL/g, pageUrl))
                    .attr('target', login.isNewWindow ? '_blank' : '');
            }

            if (logout) {
                $authBtnLogout
                    .attr('href', logout.url.replace(/PAGEURL/g, pageUrl))
                    .attr('target', logout.isNewWindow ? '_blank' : '');
            }

            if (signUp) {
                $authBtnSignup
                    .attr('href', signUp.url.replace(/PAGEURL/g, pageUrl))
                    .attr('target', signUp.isNewWindow ? '_blank' : '');
            }
        }
    }


    function showLogin(response) {
        var locationHref = document.location.href;

        $authUsername.text('');

        $headMenu.removeClass('dx-authed dx-auth-loading');
    }


    function showUser(response) {
        if (response) {
            $authUsername
                .text(response.firstName);

            $headMenu
                .removeClass('dx-auth-loading')
                .addClass('dx-authed');

        } else {
            console.error('Missing response.');
        }
    }


    // Detect login.
    var cookieName = 'developerpaypalcom',
        ca = document.cookie.split(';'),
        cookie;

    for (var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cookieName) === 0) {
            cookie = decodeURIComponent(decodeURIComponent(c.substring(cookieName.length + 1, c.length)));
        }
    }

    if (cookie) {
        cookie = JSON.parse(cookie);
        seedButtons(cookie);
        if (cookie.loggedIn) {
            showUser(cookie);
        } else {
            showLogin(cookie);
        }
    } else {
        $headMenu
            .removeClass('dx-auth-loading');
    }


    function supportsSvg () {
        return document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1');
    }

    // SVG fallback solution:
    // Usage: Prior to an inline SVG, add a fallback IMG tag with class=svg-fallback plus "data-src" and "data-alt" in place of "src" and "alt".
    // E.g., <img class="svg-fallback" data-src="my-image.png" data-alt="my image">
    // If SVG is not supported by the browser, then a script will convert all the image "data-src" attributes to "src" and
    // add a ".no-svg" class to the HTML tag so we can hide the inline SVGs with CSS.
    $('html').addClass(function () {
        var htmlClass = 'svg';

        if (!supportsSvg()) {
            // activate SVG fallback image tags
            $('.svg-fallback')
                .attr('alt', function () {
                    var $this = $(this),
                        alt = $this.data('alt');

                    $this.removeAttr('data-alt');

                    return alt;
                })
                .attr('src', function () {
                    var $this = $(this),
                        src = $this.data('src');

                    $this.removeAttr('data-src');

                    return src;
                });

            // add no-svg class to html element
            htmlClass = 'no-svg';
        } else {
            // making SVG responsive: http://demosthenes.info/blog/744/Make-SVG-Responsive
            $('.dx-content svg:not(.dx-svg-not-responsive)').wrap(function () {
                var vb = this.getAttribute('viewBox').split(' '),
                    // proportion = viewbox height / viewbox width
                    pad = ((vb[3] / vb[2]) * 100) + '%';

                return '<span class="dx-svg-wrap" style="padding-top:' + pad + '"></span>'
            });
        }

        return htmlClass;
    });

    dx.setupCodeLanguagesSwitcher();
    dx.loadAnalytics();
});
