var main = $(".method");
var heading = $(".method h3");
var endpoint = $(".method h3");;
var snippet_pre = $(".method pre")[0]; var request = $(snippet_pre).contents();
var snippet_pre2 = $(".method pre")[1]; var response = $(snippet_pre2).contents();
var description = $(".method p");
var parameter = $('table.params');
var param_table = $('table.params')[0];
var parameter_name = $(param_table).find('tr td:first-child > span:first-child');
var parameter_type = $(param_table).find('tr td:first-child > span:nth-child(2)');
var parameter_desc = $(param_table).find('tr td:nth-child(2)');
var response_table = $('.method table')[1];
var response_param = $(response_table); 


if (checkExistance(main)){
    main.addClass("api_main");   
}
if (checkExistance(heading)){
    heading.addClass("api_heading");
}
if (checkExistance(endpoint)){
    endpoint.addClass("api_endpoint");
}
if (checkExistance(request)){
    request.addClass("api_request_snippet");
}
if (checkExistance(response)){
    response.addClass("api_response_snippet");
}
if (checkExistance(description)){
    description.addClass("api_description");
}
if (checkExistance(parameter)){
    parameter.addClass("api_parameter");
}
if (checkExistance(parameter_name)){
    parameter_name.addClass("api_parameter_name");
}
if (checkExistance(parameter_type)){
    parameter_type.addClass("api_parameter_type");
}
if (checkExistance(parameter_desc)){
    parameter_desc.addClass("api_parameter_desc");
}
if (checkExistance(response_param)){
    response_param.addClass("api_response_parameter");
}

function checkExistance(elem){
    if(elem){
        return true;
    }else{
        return false;
    }
}
