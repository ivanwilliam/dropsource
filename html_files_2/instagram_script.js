var section = $("section:first-child section");
var heading = $("section:first-child section header h2");
var endpoint = $("section:first-child section .card-info code");
var response = $("section:first-child section .ep-response code");
var description = $("section:first-child section .ep-description p");
var parameter_name = $("section:first-child section .ep-description table:nth-child(3) tr th"); 
var parameter_desc = $("section:first-child section .ep-description table:nth-child(3) tr td");


if (checkExistance(section)){
    section.addClass("api_main");   
}
if (checkExistance(heading)){
    heading.addClass("api_heading");
}
if (checkExistance(endpoint)){
    endpoint.addClass("api_endpoint");
}
if (checkExistance(response)){
    response.addClass("api_response_snippet");
}
if (checkExistance(description)){
    description.addClass("api_description");
}
if (checkExistance(parameter_name)){
    parameter_name.addClass("api_parameter_name");
}
if (checkExistance(parameter_desc)){
    parameter_desc.addClass("api_parameter_desc");
}

function checkExistance(elem){
    if(elem){
        return true;
    }else{
        return false;
    }
}

