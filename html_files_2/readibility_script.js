var main = $('.doc-block .resource');
var heading = $('.doc-block .resource h3');
var endpoint = $('.doc-block .resource h3');
a = $(".methods .method:nth-child(2)");
b = a[0];
descriptionArr = $(b).contents();
var out = description = '';
for(i=1; i<descriptionArr.length; i++){  
    e = descriptionArr[i];
    f = $(b).find('h6')[0]
    if (e==f){
      break;
    }
    out+= $(descriptionArr[i]).text()+" ";
    $(descriptionArr[i]).remove();
}
description = out.trim();
var paramTable = $(".methods .method:nth-child(2) table");
var parameter = paramTable[0];
var parameter_name = $(parameter).find('td:nth-child(1)');
var parameter_type = $(parameter).find('td:nth-child(2)');
var parameter_desc = $(parameter).find('td:nth-child(3)');


if (checkExistance(main)){
    main.addClass("api_main");   
}
if (checkExistance(heading)){
    heading.addClass("api_heading");
}
if (checkExistance(endpoint)){
    endpoint.addClass("api_endpoint");
}
if (checkExistance(description)){
    $("<div class='api_description'>"+description+"</div>").insertAfter(descriptionArr[0])
}
if (checkExistance(parameter)){
    parameter.addClass("api_parameter");
}
if (checkExistance(parameter_name)){
    parameter_name.addClass("api_parameter_name");
}
if (checkExistance(parameter_type)){
    parameter_type.addClass("api_parameter_type");
}
if (checkExistance(parameter_desc)){
    parameter_desc.addClass("api_parameter_desc");
}

function checkExistance(elem){
    if(elem){
        return true;
    }else{
        return false;
    }
}


