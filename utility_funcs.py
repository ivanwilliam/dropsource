import pickle
import gzip

def save_classifier_data(data, file_path):
	f = gzip.open( file_path, 'w' )
	pickle.dump( data , f)
	f.close()
def load_classifier_data(file_path):
	f = gzip.open(file_path)
	output = pickle.load( f )
	f.close()
	return output