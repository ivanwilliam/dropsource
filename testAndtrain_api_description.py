import re
import glob
import urllib
import random
from utility_funcs import *
from urllib2 import urlopen
from description_funcs import *
from bs4 import BeautifulSoup
from sklearn import linear_model
from sklearn.metrics import f1_score
from sklearn.pipeline import FeatureUnion
from sklearn.feature_selection import chi2
from sklearn.metrics import accuracy_score
from scipy.sparse import coo_matrix, hstack
from sklearn.feature_selection import SelectKBest
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

html_folder1 = 'Labelled html files/'
html_files1 = glob.glob('%s*.html' % html_folder1)
html_files1 = sorted(html_files1)
html_folder2 = 'html_files/'
html_files2 = glob.glob('%s*.html' % html_folder2)
html_files2 = sorted(html_files2)
html_folder3 = 'html_files_2/'
html_files3 = glob.glob('%s*.html' % html_folder3)
html_files3 = sorted(html_files3)

html_files = html_files1 + html_files2 + html_files3
# html_files = [html_files[-1]]
soup_main = []
html_pages = []
api_description = []
all_code_tags = []
desc_candidates = []
y = []


def test_and_train(text,y):
	c = list(zip(text, y))
	random.shuffle(c)
	text,y = zip(*c)	
	t_train = int(0.8*len(y))
	text_train = text[0:t_train]
	text_test = text[t_train+1:]
	y_train = y[0:t_train]
	y_test = y[t_train+1:]
	# clf = RandomForestClassifier()
	clf = linear_model.SGDClassifier()
	# v = TfidfVectorizer(ngram_range=(2,6))
	v = CountVectorizer(ngram_range=(1,4))
	kbest = SelectKBest(chi2, k=50)
	X_train = v.fit_transform(text_train)
	X_train = kbest.fit_transform(X_train, y_train)
	clf.fit(X_train.toarray(), y_train)
	X_test = v.transform(text_test) 
	X_test = kbest.transform(X_test)
	pred = clf.predict(X_test.toarray())
	accr = accuracy_score(y_test,pred)
	print "Accuracy score:"
	print accr
	print "Precision fscore score:"
	print(precision_recall_fscore_support(y_test, pred))
	print 'F1 score'
	print (f1_score(y_test, pred, average=None))
	print(y_test, pred)
	tp = 0
	fp = 0
	tn = 0
	for i in range(len(pred)):
		if pred[i] == y_test[i]:
			if y_test[i]==1:
				tp = tp + 1
		else:
			if pred[i]==1:
				fp = fp+1
	print(tp, fp)


def get_all_text(soup, data = []):
	if type(soup).__name__ == 'NavigableString':
		data.append(type(soup).__name__ +"  --> " + soup)
		return
	else:
		try:
			if 'children' in dir(soup):
				for ch in soup.children:
					get_all_text(ch, data)
		except Exception as ex:
			print(ex)
			global y
			y = ch
	return



# print 'Extracting Candidates . . .'
# for f in html_files: 
# 	page = urllib.urlopen(f).read()
# 	soup = BeautifulSoup(page, "lxml")
	
# 	description = soup.findAll(True, {'class': 'api_description'})
# 	for d in description:
# 		api_description.append(d.getText())

# 	soup_main.append(soup)
# 	html_text = soup.findAll(text=True)
# 	html_text = ' \n '.join(html_text)

# 	tag_name = ''
# 	attrs_main = {}
# 	for child in soup.recursiveChildGenerator():
# 		name = getattr(child, "name", None)
# 		attrs = getattr(child,"attrs",{})
# 		classes = []	
# 		if name is None and not child.isspace():
# 			if tag_name == "p":
# 				try:
# 					if 'api_description' in attrs_main['class']:
# 						attrs_main['class']=[]
# 						y.append(1)
# 					else:
# 						y.append(0)
# 				except:
# 					y.append(0)
# 				desc_candidates.append({
# 					'attrs':attrs_main,
# 					'tag':tag_name,
# 					'text':child,
# 					'parent':child.parent,
# 					'html_text':html_text
# 					})
# 		tag_name = name
# 		if attrs:
# 			attrs_main = attrs
# text=[]
# for dc in desc_candidates:
# 	text.append(dc['text'])


pf = param_funcs()
desc_candidates_new = []
y_new = []
text=[]
print 'Extracting Candidates'
for f in html_files: 
	page = urllib.urlopen(f).read()
	soup = BeautifulSoup(page, "lxml")
	(alln,disc,dc) = pf.extract_candidates(soup)
	desc_candidates_new = desc_candidates_new + dc
	y_new = y_new + disc
print 'Extracting features'
features = features_extraction(desc_candidates_new)
for dcn in desc_candidates_new:
	text.append(dcn['text'])

# asd


clf = RandomForestClassifier(verbose=0)
v = DictVectorizer(sparse=False)
v_count = CountVectorizer(ngram_range=(1,3))
kbest_count = SelectKBest(chi2, k=30)

X_count = v_count.fit_transform(text)
X_count = kbest_count.fit_transform(X_count, y_new)
X = v.fit_transform(features)
combined_features = hstack((X, X_count),format='csr')

clf = clf.fit(combined_features, y_new)
save_classifier_data([ clf, v, v_count , kbest_count ], 'models/api_description_clf_new(22-3-16).gzip')
	



# clf = linear_model.SGDClassifier()
# v = CountVectorizer(ngram_range=(1,4))
# kbest = SelectKBest(chi2, k=50)
# X = v.fit_transform(text)
# X = kbest.fit_transform(X, y)
# clf.fit(X.toarray(), y)

# print 'Candidates Extracted'

# clf = RandomForestClassifier(verbose=0)
# v = DictVectorizer(sparse=False)
# v_count = CountVectorizer(ngram_range=(1,3))
# kbest_count = SelectKBest(chi2, k=50)

# print 'Extracting Features . . .'
# candidates_features, count_features_text = features_extraction(endpoint_candidates)
# print 'Features Extracted'

# count_features = v_count.fit_transform(count_features_text)
# count_features = kbest_count.fit_transform(count_features, y)
# X = v.fit_transform(candidates_features)
# combined_features = hstack((X, count_features),format='csr')

# asd

# save_classifier_data([ clf, v, kbest ], 'models/api_description_clf.gzip')