import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
import os
from parameter_funcs import *
from utility_funcs import *
from scipy.sparse import coo_matrix, hstack
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import FeatureUnion


html_folder1 = 'Labelled html files/'
html_files1 = glob.glob('%s*.html' % html_folder1)
html_files1 = sorted(html_files1)
html_folder2 = 'html_files/'
html_files2 = glob.glob('%s*.html' % html_folder2)
html_files2 = sorted(html_files2)

html_files = html_files1 + html_files2
# html_files = [html_files[-1]]
soup_main = []
html_pages = []
api_heading = []
api_endpoint = []
api_description = []
all_code_tags = []
endpoint_candidates = []
y = []



def get_all_text(soup, data = []):
	if type(soup).__name__ == 'NavigableString':
		data.append(type(soup).__name__ +"  --> " + soup)
		return
	else:
		try:
			if 'children' in dir(soup):
				for ch in soup.children:
					get_all_text(ch, data)
		except Exception as ex:
			print(ex)
			global y
			y = ch
	return



pf =param_funcs()
print 'Extracting Candidates . . .'
for f in html_files: 
	page = urllib.urlopen(f).read()
	soup = BeautifulSoup(page, "lxml")
	heading = soup.findAll(True, {'class': 'api_parameter_name'})
	endpoint = soup.findAll(True, {'class': 'api_parameter_name'})
	container = soup.findAll(True, {'class': 'api_parameter_name'})
	soup_main.append(soup)
	html_text = soup.findAll(text=True)
	html_text = ' \n '.join(html_text)

        fstuff = True
        if fstuff:
                (alln,label,ec) = pf.extract_candidates(soup)
                for i in range(1,len(label)):
                        y.append(label[i])
                        endpoint_candidates.append(ec[i])


print 'Candidates Extracted'

clf = RandomForestClassifier(verbose=0)
v = DictVectorizer(sparse=False)
v_count = CountVectorizer(ngram_range=(1,3))
kbest_count = SelectKBest(chi2, k=50)

print 'Extracting Features . . .'
candidates_features, count_features_text = pf.features_extraction(endpoint_candidates)
print 'Features Extracted'

print 'Transforming count features'
count_features = v_count.fit_transform(count_features_text)
count_features = kbest_count.fit_transform(count_features, y)
X = v.fit_transform(candidates_features)
combined_features = hstack((X, count_features),format='csr')

print 'Fitting and saving'
clf = clf.fit(combined_features.toarray(), y)

parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

save_classifier_data([ clf, v, v_count , kbest_count ], parent_dir+'models/api_parameter_clf.gzip')
print 'End'
