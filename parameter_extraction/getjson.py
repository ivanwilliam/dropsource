
import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import parameter_funcs_req as pf
import json
import re

class json_extracter:
    debugflag = False
    def checkbal(self,text):
        toappb=[]
        needtoappb = False
        textlen = len(text)
        isbal = True
        tcount = 0
        sstack = []
        indices = []
        #tcount = text.find('{')
        #if self.debugflag:
        #    import ipdb 
        #    ipdb.set_trace()
        #import ipdb 
        #ipdb.set_trace()
        while (tcount < textlen and tcount != -1):
            beginc = True
            isbal = True
            foundbrace = False
            curopen = 0
            lastc = 0
            needtoappb = False
            #if len(indices)>=32:
            #    self.debugflag = True
            #if self.debugflag:
            #    import ipdb
            #    ipdb.set_trace()
            ptcount = tcount
            tcount = text[tcount:].find('{')
            if tcount == -1:
                break
            tcount = tcount + ptcount
            while isbal:
                
                ch = text[tcount]
                tcount = tcount + 1
                #if tcount == 0:
                #    import ipdb
                #    ipdb.set_trace()
                #print tcount
                if ch == '{':
                    curopen = tcount
                    if lastc > 0 and (curopen - lastc) > 100 and foundbrace:
                        #i now have something that maybe i can close
                        #so what do i do 
                        #import ipdb
                        #ipdb.set_trace()
                        if len(sstack)==1:
                            sstack.pop()
                            tcount = lastc
                            needtoappb = True
                            break
                    #import ipdb 
                    #ipdb.set_trace()
                    if not foundbrace:
                        foundbrace = True
                    if beginc:
                        startc = tcount-1
                        beginc = False
                    #print ch
                    sstack.append(ch)
                    #print tcount
                    #raw_input("added a brace")
                else:
                    if ch == '}':
                        lastc = tcount
                        #import ipdb
                        #ipdb.set_trace()
                        #if not foundbrace:
                        #    foundbrace = True
                        if not sstack == []:
                            #print sstack
                            sstack.pop()
                            #print sstack
                            #raw_input("popped a brace")
                        else:
                            isbal = False
                            #raw_input("unbalanced")
                        if isbal and sstack == []:
                            break
                    if tcount == textlen:
                        break
            endc = tcount
            #if self.debugflag:
            #    import ipdb
            #    ipdb.set_trace()
            #raw_input("exited once")
            #print startc
            #print endc
            if isbal and foundbrace and sstack == []:
                indices.append((startc,endc))
                toappb.append(needtoappb)

        return indices,toappb



    def skip_script(self,leafnodes):
        all_leaf_text = []
        for leaf in leafnodes:
            if getattr(leaf,"parent",None):
                if type(leaf.parent).__name__ == 'Tag':
                    if leaf.parent.name == 'script' or leaf.parent.name == 'style':
                        continue
                    else:
                        attrs_ = leaf.parent.attrs
                        if attrs_:
                            if 'id' in attrs_:
                                if attrs_['id'] == 'project-info':
                                    continue
                            if 'style' in attrs_:
                                if 'display: none;' in attrs_['style']:
                                    continue
                        all_leaf_text.append(leaf)
                elif leaf.parent.name == 'style':
                    continue
                else:
                    attrs_ = leaf.parent.attrs
                    if 'style' in attrs_:
                        val = attrs_['style']
                        if not ('display' in val and 'none' in val):
                            all_leaf_text.append(leaf)
                    else:
                        all_leaf_text.append(leaf)
            else:
                all_leaf_text.append(leaf)
        #print 'ended extraction'
        return all_leaf_text

    def get_jsons(self,leafnodes):
        nln = self.skip_script(leafnodes)
        text = ' '.join(nln)
        text = text.replace(', ...]','}]')
        st,toappb = self.checkbal(text)
        text_bits = []
        toapp = True
        for idx,stt in enumerate(st):
            toapp = True
            text_bit = text[stt[0]:stt[1]]
            if toappb[idx]:
                text_bit = text_bit + '}'
            if text_bit.find('} \n') > 0:
                text_bit = text_bit.replace('} \n','}}')
                #print "on text bit checking bal again"
                st_a,toappb_a = self.checkbal(text_bit)
                #print "checked bal"
                for idx_a,st_aa in enumerate(st_a):
                    text_bit2 = text_bit[st_aa[0]:st_aa[1]]
                    if toappb_a[idx_a]:
                        text_bit2 = text_bit2 + '}'
                    text_bits.append(text_bit2)
                    toapp = False
            if toapp:
                text_bits.append(text_bit)
            
            #print text_bit
            #raw_input("next")
        return (nln,text,st,text_bits)

    def get_valid_json(self,text_bits):
        valjson = []
        corr_tb = []
        pot_tb = []
        print "getting valid json"
        for tb in text_bits:
            try:
                tbb = self.clean_potential_json(tb)
                pot_tb.append(tbb)
                valjson.append(json.loads(tbb))
                corr_tb.append(tb)
            except:
                pass
        return (valjson,corr_tb,pot_tb)

    def get_valid_json_for_file_f(self,f):
        (page,soup,leafnodes) = self.get_soup_leafnodes(f)
        (nln,text,st,text_bits) = self.get_jsons(leafnodes)
        (valjson,corr_tb,pot_tb) = self.get_valid_json(text_bits)
        return (valjson,corr_tb,pot_tb)
    
    def get_json_for_file_f(self,f):
        (page,soup,leafnodes) = self.get_soup_leafnodes(f)
        (nln,text,st,text_bits) = self.get_jsons(leafnodes)
        return text_bits

    def get_soup_leafnodes(self,f):
        page = urllib.urlopen(f).read()
        soup = BeautifulSoup(page, "lxml")
        pfp = pf.param_funcs_req()
        leafnodes = []
        pfp.depth_first_traversal(soup,leafnodes)
        return (page,soup,leafnodes)

    def get_files(self):
        html_folder1 = '../Labelled html files/'
        html_files1 = glob.glob('%s*.html' % html_folder1)
        html_files1 = sorted(html_files1)
        html_folder2 = '../html_files/'
        html_files2 = glob.glob('%s*.html' % html_folder2)
        html_files2 = sorted(html_files2)
        html_folder3 = '../html_files_2/'
        html_files3 = glob.glob('%s*.html' % html_folder3)
        html_files3 = sorted(html_files3)

        html_files = html_files1 + html_files2 + html_files3
        return html_files

    def clean_potential_json(self,toclean):
        #print "cleaning pot json"
        #print toclean
        toclean = toclean.replace('\n','')
        ex = re.compile('.*?<a href=(.*?)</a>"')
        #print "fixing the href stuff"
        #if 'amount_range currency Amount_range associated with this term.' in toclean:
        #    import ipdb
        #    ipdb.set_trace()

        matches = ex.findall(toclean)
        for match in matches:
            torep = match 
            torep = torep.replace('"','')
            torep = torep.replace('\\','')
            toclean=toclean.replace(match,torep)
        #todo get a nice regex for this cuz spaces may vary
        toclean = toclean.replace(', }','}')
        #print "replaced ,}"
        ex = re.compile('([0-9]\s+\.\s+[0-9])')
        matches = ex.findall(toclean)
        #print "fixing spaces in decimals"
        for match in matches:
            torep = match 
            torep = torep.replace(' ','')
            toclean = toclean.replace(match,torep)
        ex = re.compile('("\s*")')
        (toclean,num) = ex.subn('","',toclean)
        #print "fixing apostrophes"
        ex = re.compile(',\s*\}')
        (toclean,num) = ex.subn('}',toclean)
        #print "fixing more ,}"
        ex = re.compile('\[\s*//\s*.*\]')
        matches = ex.findall(toclean)
        #print "fixing last thing"
        for match in matches:
            torep = match
            torep = '"'+torep+'"'
            toclean = toclean.replace(match,torep)
        #ex = re.compile(
        #print "cleaned pot json"
        return toclean
        

    def do_all_files(self,get_errors=True):
        files = self.get_files()
        error_files = []
        for f in files:
            print "On file %s" % f
            vj,ig,nore = self.get_valid_json_for_file_f(f)
            #print len(vj)
            import pprint
            pprint.pprint(vj)
            print len(vj)
            if len(vj) == 0: 
                print f
                if get_errors == True:
                    error_files.append(f)
                else:
                    raw_input("next")
        return error_files


