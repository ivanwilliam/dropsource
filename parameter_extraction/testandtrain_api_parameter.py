import testAndtrain_api_parameter_req as test_train_req
import testAndtrain_api_parameter_resp as test_train_resp
import testAndtrain_api_parameter_desc as test_train_desc


if __name__=='__main__':
    test_train_req.testandtrain_param_req()
    test_train_resp.testAndtrain_api_param_resp()
    test_train_desc.testandtrain_param_desc()
