import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
import os
from parameter_funcs_desc import *
from utility_funcs import *
from scipy.sparse import coo_matrix, hstack
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.pipeline import FeatureUnion
import get_api_param_req 
import get_api_param_resp
import numpy as np
import ipdb

html_folder1 = '../Labelled html files/'
html_files1 = glob.glob('%s*.html' % html_folder1)
html_files1 = sorted(html_files1)
html_folder2 = '../html_files/'
html_files2 = glob.glob('%s*.html' % html_folder2)
html_files2 = sorted(html_files2)

html_files = html_files1 + html_files2
# html_files = [html_files[-1]]
soup_main = []
html_pages = []
api_heading = []
api_endpoint = []
api_description = []
all_code_tags = []
endpoint_candidates = []
y = []



def get_all_text(soup, data = []):
	if type(soup).__name__ == 'NavigableString':
		data.append(type(soup).__name__ +"  --> " + soup)
		return
	else:
		try:
			if 'children' in dir(soup):
				for ch in soup.children:
					get_all_text(ch, data)
		except Exception as ex:
			print(ex)
			global y
			y = ch
	return

def testandtrain_param_desc(folder_loc='/parameter_extraction/models/',classifier_name='api_parameter_desc_clf.gzip'):

        pf =param_funcs_desc()
        print 'Extracting Candidates . . .'
        filecount = 0
        all_res_resp =[]
        all_res_req = []
        fid = []
        
        for f in html_files:
                #f='../Labelled html files/v1_estimates_price Developer Platform | Uber.html'
                #get the resp and req parameters for this 
                
                res_resp = get_api_param_resp.get_api_param_resp(f)
                res_req = get_api_param_req.get_api_param_req(f)
                all_res_req.append(res_req)
                all_res_resp.append(res_resp)

                page = urllib.urlopen(f).read()
                soup = BeautifulSoup(page, "lxml")
                tolookfor = ['api_parameter_desc','api_response_parameter_desc']
                heading = soup.findAll(True, {'class':tolookfor[0]})
                heading = heading + soup.findAll(True,{'class':tolookfor[1]})
                endpoint = soup.findAll(True, {'class': tolookfor[0]})
                endpoint = endpoint+ soup.findAll(True,{'class':tolookfor[1]})
                container = soup.findAll(True, {'class': tolookfor})
                container = container + soup.findAll(True, {'class':tolookfor[1]})
                soup_main.append(soup)
                html_text = soup.findAll(text=True)
                html_text = ' \n '.join(html_text)

                fstuff = True
                if fstuff:
                        (alln,label,ec) = pf.extract_candidates(soup)
                        #import ipdb
                        #ipdb.set_trace()
                        for i in range(1,len(label)):
                                fid.append(filecount)
                                y.append(label[i])
                                endpoint_candidates.append(ec[i])
                #break
                
                filecount = filecount+1
                print "%d Done" % filecount
                #if filecount > 4:
                #        break


        print 'Candidates Extracted'

        clf = RandomForestClassifier(verbose=0)
        v = DictVectorizer(sparse=False)
        v_count = CountVectorizer(ngram_range=(1,3))
        kbest_count = SelectKBest(chi2, k=50)

        #ipdb.set_trace()
        print 'Extracting Features . . .'
        candidates_features, count_features_text = pf.features_extraction(endpoint_candidates,y,all_res_req,all_res_resp,fid)
        print 'Features Extracted'

        #print 'Transforming count features'
        #count_features = v_count.fit_transform(count_features_text)
        #count_features = kbest_count.fit_transform(count_features, y)
        X = v.fit_transform(candidates_features)
        #combined_features = hstack((X, count_features),format='csr')

        print 'Fitting and saving'
        #clf = clf.fit(combined_features.toarray(), y)
        clf = clf.fit(X,y)

        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

        save_classifier_data([ clf, v, v_count , kbest_count ], parent_dir+folder_loc+classifier_name)
        print 'End'
        return (y,endpoint_candidates,candidates_features,res_req,res_resp,clf,X,v.get_feature_names())

if __name__ == '__main__':
        (y,ecs,cfs,rrq,rrs,clf,X,xnames)=testandtrain_param_desc()
#        importances = clf.feature_importances_
#        std = np.std([clf.feature_importances_ for tree in clf.estimators_],
#             axis=0)
#        indices = np.argsort(importances)[::-1]

        # Print the feature ranking
 #       print("Feature ranking:")

  #      for f in range(X.shape[1]):
   #             print("%d. feature %d (%f) %s" % (f + 1, indices[f], importances[indices[f]], xnames[indices[f]]))
                
