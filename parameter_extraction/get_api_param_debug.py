import get_api_param_req
import get_api_param_resp
import get_api_param_desc
import json
import glob  
import pprint


if __name__=='__main__':
    html_folder1 = '../Labelled html files/'
    html_files1 = glob.glob('%s*.html' % html_folder1)
    html_files1 = sorted(html_files1)
    html_folder2 = '../html_files/'
    html_files2 = glob.glob('%s*.html' % html_folder2)
    html_files2 = sorted(html_files2)
    html_files3 = glob.glob('%s*.html' % '../html_files_2/')
    html_files3 = sorted(html_files3)

    html_files = html_files1 + html_files2 + html_files3
    html_files = html_files3
    totfiles = len(html_files)
    for idx,html_file in enumerate(html_files):
        print "on file %d of %d" % (idx,totfiles)

        print "File name %s" % html_file
        (res_desc,alln,disc,param_candidates,cf,ps,res_resp,res_req) = get_api_param_desc.get_api_param_desc_debug(html_file)
        print "Response Params" 
        print json.dumps(res_resp)
        pprint.pprint(res_resp)
        print "Request Params"
        print json.dumps(res_req)
        pprint.pprint(res_req)
        #print "Parameter Descriptions"
        #print json.dumps(res_desc)
        go_on = raw_input("next or exit (n or e)") 
        if go_on == 'e':
            break
