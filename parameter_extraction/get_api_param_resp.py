import sys
import urllib
from utility_funcs import *
from parameter_funcs_resp import *
from bs4 import BeautifulSoup
from urllib2 import urlopen
from scipy.sparse import coo_matrix, hstack
import glob
import json

def load_classifier_api(path):
	clf_data= load_classifier_data(path)
	clf = clf_data[0]
	v = clf_data[1]
	v_count = clf_data[2]
	kbest_count = clf_data[3]
	return clf, v, v_count, kbest_count

def get_highest_prob(prob):
	prob_candidates = []
	for p in prob:
		prob_candidates.append(p[1])
		# print p[1]
	index = prob_candidates.index(max(prob_candidates))	
	return index



#if __name__ == '__main__':
def get_api_param_resp(html_file,classifier_loc_name='models/api_parameter_resp_clf.gzip'):

#	html_file = sys.argv[1]
	#creating a class obj
	pf = param_funcs()
	# testfile1 = 'Labelled html files/The Google Maps Geocoding API|Google Maps Geocoding API|Google Developers.html'
	# testfile1 = 'Labelled html files/POST statuses_destroy_id | Twitter Developers.html'
	# testfile1 = 'Labelled html files/GET statuses_user_timeline | Twitter Developers.html'
	# page = urllib.urlopen(testfile1).read()
	page = urllib.urlopen(html_file).read()

	page = page.replace('<wbr>','')
	page = page.replace('</wbr>','')

	#print 'Loading Classifiers'
	clf, v, v_count, kbest_count = load_classifier_api(classifier_loc_name)

	soup = BeautifulSoup(page, "lxml")

	(alln,disc,param_candidates) = pf.extract_candidates(soup)

	try:	
		candidates_features, count_features_text = pf.features_extraction(param_candidates)
		count_features = v_count.transform(count_features_text)
		count_features = kbest_count.transform(count_features)
		X = v.transform(candidates_features)
		combined_features = hstack((X, count_features),format='csr')
		prob = clf.predict_proba(combined_features.toarray())
		pred = clf.predict(combined_features.toarray())
		highest_prob = get_highest_prob(prob)

		result = []
		result_low=[]
		indexes = []
		for p in range(len(prob)):
			if not prob[p][1]==0:
				indexes.append(p)
		threshold = 0.4
		for i in indexes:
			if prob[i][1]>threshold:
				result_low.append(param_candidates[i]['text'])
		for i in indexes:
			if prob[i][1]>threshold:
				p_type = ''
				for ns in param_candidates[i]['next_siblings']:
					try:
						if len(ns.string.split(' '))==1 and not ns.string=='\n':
							if ns.string in result_low:
								break
							else:
								p_type = ns.string							
								break
					except:
						pass
				result.append({
					'text' : param_candidates[i]['text'],
					'type' : p_type
					})
				# result.append(param_candidates[i]['text'])

		#print json.dumps(result)
                return result

	except Exception as e:
		# print e
		res = ['No parameter candidate found']
		#print json.dumps(res)
                return res

if __name__ == '__main__':
        html_file = sys.argv[1]
        res = get_api_param_resp(html_file)
        print json.dumps(res)
