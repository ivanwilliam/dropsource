import re
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
# import ipdb

class param_funcs_desc:
        
        #variables for this class
        debug_now =False
        label_to_find = ['api_parameter_desc','api_response_parameter_desc']  #label for positive samples
        
        def extract_prv_nxt_siblings(self,child,dirsib='p',maxsibs=6):
                # lets get the first 3 prev and next siblings
                numsib = 0
                psibs=[]
                if dirsib == 'p':
                        siblings = list(child.parent.previous_siblings)
                else:
                        siblings = list(child.parent.next_siblings)
                for sib in siblings:
                        if not sib == '\n':
                                psibs.append(sib)
                                numsib = numsib+1
                        if(numsib > maxsibs):
                                break
                #if len(psibs) == 0:
                #we actually dont care about sections right now
                #so we will do this twice
                        #go to parents parent
                cp = child
                for i in range(0,2):
                        cp = cp.parent
                        if getattr(cp,"parent",None):
                                if dirsib == 'p':
                                        siblings = list(cp.parent.previous_siblings)
                                else:
                                        siblings = list(cp.parent.next_siblings)
                                for sib in siblings:
                                        if not sib == '\n':
                                                psibs.append(sib)
                                                numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break

                return psibs 

        def extract_prev_siblings(self,child,maxsibs=6):
                return self.extract_prv_nxt_siblings(child,'p',maxsibs)

        def extract_next_siblings(self,child,maxsibs=6):
                return self.extract_prv_nxt_siblings(child,'n',maxsibs)


        def depth_first_traversal(self,soup_body,leafnodes):
                #mark that bit as visited 
                #then go ahead and visit all its unvisted childre
                try:
                        isvisted = getattr(soup_body,"visited",False)
                        if not isvisted:
                                soup_body.visited = True
                                haschildren = getattr(soup_body,"children",False)
                                if not haschildren:
                                    if not soup_body.isspace():
                                       # print soup_body 
                                        leafnodes.append(soup_body)

                                if haschildren:
                                        for soup_child in soup_body.children:
                                                self.depth_first_traversal(soup_child,leafnodes)
                except:
                    pass
                    # print "error"

        def get_parent_attr(self,node):
            attrs={}
            try:
                attrs = getattr(node.parent,"attrs",{})
                while not attrs:
                    node = node.parent
                    attrs = getattr(node.parent,"attrs",{})
            except:
                pass
            return attrs


        def extract_candidates(self,soup):
                all_nodes_labels = []
                endpoint_candidates = []
                y=[]
                html_text = soup.findAll(text=True)
                html_text = ' \n '.join(html_text)
                tag_name = ''
                attrs_main = {}
		possible_tags = ['tr','td','table','div','span']
                leafnodes_l = []
                pleaf = None
                ppleaf = None
                self.depth_first_traversal(soup,leafnodes_l)
                if leafnodes_l is not None:
                #for each leaf node 
                #to account for ... if that word is by itself and it says optional or required 
                #it is NOT what we want. 
                        for leaf in leafnodes_l:
                                leaft = leaf
                                name = getattr(leaf,"name",None)
                                attrs = getattr(leaf,"attrs",{})
                                if name is None:
                                    #then we want the name of the parent 
                                        if getattr(leaf,"parent",None):
                                                tag_name = leaf.parent.name
                                        else:
                                                tag_name = ''
                                if not attrs:
                                        attrs = self.get_parent_attr(leaf)
                                toappend = False
                                #if 'pending' == leaf:
                                #        import ipdb
                                #        ipdb.set_trace()
                                leafparent = getattr(leaf,"parent",None)
                                leafparentparent = None
                                tocheckforparenttags = False
                                parentparenttags = False
                                if leafparent:
                                        tocheckforparenttags = (leaf.parent.name == 'td' or leaf.parent.name == 'ul' or leaf.parent.name == 'li')
                                        leafparentparent = getattr(leaf.parent,"parent",None)
                                        if leafparentparent:
                                                parentparenttags = (leaf.parent.parent.name == 'td' or leaf.parent.parent.name == 'ul' or leaf.parent.parent.name == 'li')
                                                tocheckforparenttags = tocheckforparenttags or parentparenttags
                               
                                if tag_name in possible_tags:
                                        try:
                                                if self.label_to_find[0] in attrs['class'] or 'param' in attrs['class'] or self.label_to_find[1] in attrs['class']:
                                                   #     if has_opt_req:
                                                   #             y.append(0)
                                                   #     else:
                                                        
                                                        
                                                        if tocheckforparenttags:
                                                                        #print leaf
                                                                        #import ipdb
                                                                        #ipdb.set_trace()
                                                                        
                                                                        #i want to check if the parent node didn't have multiple candidates 
                                                                        if len(list(leaf.parent.children)) > 1:
                                                                        
                                                                                #I want the text of the parent 
                                                                                leaft = leaf.parent.text
                                                                                y.append(1)
                                                                        elif parentparenttags:
                                                                                if len(list(leaf.parent.parent.children)) > 1:
                                                                                   leaft = leaf.parent.parent.text
                                                                                   y.append(1)
                                                                                else:
                                                                                   y.append(1)
                                                                        else:
                                                                                y.append(1)
                                                        
                                                        else:
                                                                y.append(1)
                                                               
                                                        #y.append(1)
                                                        if self.debug_now:
                                                                import ipdb
                                                                ipdb.set_trace()
                                                else:
                                                        y.append(0)
                                        except:
                                                y.append(0)
                                        toappend = True
                                 #       print "append"
                                else:
                                        xleaf = leaf.strip()
                                        #banking on the fact that descriptions are generally short 
                                        #lets say 20 words max 
                                        num_words = len(xleaf.split())
                                        if num_words <= 100: #this is purely experimental!!! 
                                        #m = re.search('^[A-z,\[\],_]+$',xleaf)
                                        
                                        #if m:
                                                try:
                                                        if self.label_to_find[0] in attrs['class'] or 'param' in attrs['class'] or self.label_to_find[1] in attrs['class']:
                                                                #for things that somehow get broken down 
                                                                #if leaf.parent.name == 'td' or tag_name == 'p' or leaf.parent.name == 'li' or leaf.parent.name == 'ul':
                                                                if tocheckforparenttags or tag_name == 'p':
                                                                        #i want to check if the parent node didn't have multiple candidates 
                                                                        #print leaf
                                                                        #import ipdb
                                                                        #ipdb.set_trace()
                                                                        
                                                                        
                                                                        if (len(list(leaf.parent.children))) > 1:
                                                                                #I want the text of the parent 
                                                                                leaft = leaf.parent.text
                                                                                y.append(1)
                                                                        elif parentparenttags:
                                                                                if len(list(leaf.parent.parent.children)) > 1:
                                                                                   leaft = leaf.parent.parent.text
                                                                                   y.append(1)
                                                                                else:
                                                                                   y.append(1)
                                                                        else:
                                                                                y.append(1)
                                                                else:
                                                                        y.append(1)
                                                        else:
                                                                y.append(0)
                                                except:
                                                        y.append(0)
                                                toappend = True
                                  #      print "append"
                                if toappend:
                           #             dontcontinue = False
                            #            if pleaf:
                             #                   
                              #                  if "example" in pleaf or "Example" in pleaf:
                                                       # import ipdb 
                                                       # ipdb.set_trace()
                               #                         dontcontinue = True
                                #        if ppleaf: 
                                 #               if "example" in ppleaf or "Example" in ppleaf:
                                  #                      dontcontinue = True
                                   #     if not dontcontinue:        
                                                #if len(leaft.split()) == 1:
                                                
                                                 #       #skip this 
                                                 #       y = y[:-1]
                                                  #      continue
                                    #            if leaft == pleaf:
                                     #                   y = y[:-1]
                                      #                  continue
                                        
                                        
                                        endpoint_candidates.append({
                                                        'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaft,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)
                                        })
                                all_nodes_labels.append({'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaft,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)})
                                pleaf = leaft
                                ppleaf = pleaf
                                if self.debug_now:
                                        import ipdb 
                                        ipdb.set_trace()

                else:
                    pass
                        # print "List is not iterable"
                if len(y) == len(endpoint_candidates):
                    pass
                        # print "all good so far"


                return (all_nodes_labels,y,endpoint_candidates)
        
        def get_child_string(self,ch):
                #import ipdb
                #ipdb.set_trace()
                toret = []
                try:
                        if ch is not []:
                        
                                if type(ch).__name__ is not 'NavigableString':
                                        #i want all the children 
                                        if getattr(ch,"children",None):
                                                chchild = list(ch.children)
                                                if (len(chchild)>= 1):
                                                        for chi in chchild:
                                                                if not getattr(chi,"text",None) == None:
                                                                        if not chi.text == '\n' and not chi.text == '':
                                                                                toret.append(chi.text)
                                                                else:
                                                                        if not chi == '\n' and not chi == '':
                                                                                toret.append(chi)
                                        else:
                                                if getattr(ch,"text",None):
                                                        toret.append(ch.text)
                                else:
                                        toret.append(ch)
                except:
                        print 'exception occured'
                        import ipdb
                        ipdb.set_trace()
                        pass
                
                return toret
            



        def features_extraction(self,endpoint_candidates,ys,all_res_req,all_res_resp,fids):
                ccount = 0
                features = []
                largest_occurence = 0
                for idx,e in enumerate(endpoint_candidates):
                        #if ys[idx] == 1:
                        #        import ipdb 
                        #        ipdb.set_trace()
                        text = e['text']
                        f={}
                        set_params = True
                        if idx > 0:
                                if fids[idx] == fids[idx-1]:
                                        set_params = False
                        if set_params:
                                res_req = all_res_req[fids[idx]]
                                res_resp = all_res_resp[fids[idx]]
                                all_params = res_req + res_resp
                                all_param_names = []
                                for ap in all_params:
                                        all_param_names.append(ap['text'])
                        pparam = None
                        nparam = None
                        tag = e['tag']
                        parent = e['parent']
                        
                        html_text = e['html_text']
                        previous_sibling = e['previous_siblings']
                        if len(previous_sibling) > 0:
                                previous_sibling = previous_sibling[0]
                        next_sibling = e['next_siblings']
                        if len(next_sibling) > 0:
                                next_sibling = next_sibling[0]
                        #if(text == u'Optional reference that identifies the manifest. Example: "Order #690"'):
                        #        import ipdb; 
                        #        ipdb.set_trace();

                        f['has_tag_%s'%tag] = 1

                        try:
                                classes = e['attrs']['class']
                                for c in classes:
                                        if c==self.label_to_find[1] or c==self.label_to_find[0] or c == "api_response_parameter_name" or c == "api_parameter_name":
                                                pass
                                        else:
                                                f['has_class_%s'%c] = 1
                        except:
                                pass

                        #so this is useful we want to find the lenght of 
                        #the string under consideration 
                        
                        if len(text)<10:
                                f['length_smaller_than_10']=1
                        elif len(text)>10 and len(text)<20:
                                f['length_in_between_10_and_20'] = 1
                        elif len(text)>20 and len(text)<30:
                                f['length_in_between_20_and_30'] = 1
                        elif len(text)>30 and len(text)<40:
                                f['length_in_between_30_and_40'] = 1
                        elif len(text)>40 and len(text)<50:
                                f['length_in_between_40_and_50'] = 1
                        elif len(text)>50 :
                                f['length_greater_than_50'] = 1

                        #parameters should not have slashes 
                        #> slashes = < likelihood of being a param 
                        number_of_slash = text.count('/')
                        if number_of_slash<3:
                                f['slashes_less_than_3'] = 1
        
                        #lets also look for query or parameter for prev siblings 
                        has_words_query_param = 0 #default 
                        word_dist = 100 #some super high value here 
                        #if text == 'false if the delivery is ongoing, and you can expect additional updates.':
                        #        import ipdb 
                        #        ipdb.set_trace()
                        
			for idxx,psib in enumerate(e['previous_siblings']):
                                psib = self.get_child_string(psib)
                                #print "okay"
                                if len(psib) >= 1:
                                        psib = ''.join(psib)
                                else:
                                        psib = ''
				try:
					if psib.lower.find('query') or psib.lower.find('parameter')>=0:
						has_words_query_param = 1
                                                word_dist = idxx
						break
				except:
					pass
                        f['psib_contains_query_param'] = has_words_query_param
                        if(word_dist <= 5): #kind of like assuming at most i'll have 5 parameters for something 
                                f['query_param_less_than_5_psibs_away']=1
                        else:
                                f['query_param_less_than_5_psibs_away']=0
			
                        #lets also look for the words optional or required around this 
                        has_optional_required = 0 #default 
                        for psib in e['next_siblings']:
                                psib = self.get_child_string(psib)
                                #print "okay 2"
                                if len(psib) >= 1:
                                        psib = ''.join(psib)
                                else:
                                        psib = ''
                                try:
                                        if psib.lower.find('optional') or psib.lower.find('required')>=0:
                                                has_optional_required=1
                                                break
                                except:
                                        pass
                                #print "okay3"
                                #if idx == 3:
                                #        import ipdb
                                #        ipdb.set_trace()
                        f['psib_has_optional_required']=1
                        
                        has_req_opt = re.compile('\[*required\]*|\[*optional\]*')
                        m = has_req_opt.findall(text)
                        if m:
                                f['has_req_opt']=1
                        


                        #this is a test thing. need to check this in code 
                        #i am super hungry and have no CANDY!!!!!! 
                        #yes :P 
                        #man get food after this 
                        # okay so this guy's psibs should have the parameter we're looking for 
                        #super strong indicator right!!!! 
                        f['previous_sibling_is_param']=0
                        f['next_sibling_is_param'] = 0
                        '''if ys[idx]==1:
                                print text
                                #raw_input("pay attention")
                        if text == 'Optional reference that identifies the manifest. Example: "Order #690"':
                                import ipdb
                                ipdb.set_trace()
                        if text == 'Name of the place where the courier will make the pickup. Example: "Kitten Warehouse"':
                                import ipdb
                                ipdb.set_trace()'''
                        # you always want the sibling closest so the pidx should be like the least
                        pparam_ind = 100
                        nparam_ind = 100
                        pfound = False 
                        nfound = False
                        for pidx,psib in enumerate(e['previous_siblings']):
                                psib = self.get_child_string(psib)
                                for ppsib in psib:
                                        if ppsib in all_param_names and pparam_ind > pidx:
                                                f['previous_sibling_is_param']=1
                                                pparam = ppsib
                                                pparam_ind = pidx
                                                #print "Previous Param Found"
                                                #print text 
                                                #print pparam
                                                pfound = True 
                                                break
                                if pfound:
                                        break

                        for pidx,psib in enumerate(e['next_siblings']):
                                psib = self.get_child_string(psib)
                                for ppsib in psib:
                                        if ppsib in all_param_names and nparam_ind > pidx:
                                                f['next_sibling_is_param']=1
                                                nparam = ppsib
                                                nparam_ind = pidx
                                                #print "Next Param Found"
                                                #print text 
                                                #print nparam
                                                nfound = True
                                                break
                                if nfound:
                                        break
                                

                        e['pparam']=pparam
                        e['nparam']=nparam
                        if ys[idx] == 1:
                                todebug = 'n'
                                #todebug = raw_input('debug?')
                                if todebug == 'd':
                                        import ipdb
                                        ipdb.set_trace()

                    
                            


                        features.append(f)


                count_candidates = []
		fstuff = False
		tackone = False #now i'm tacking on multiple ones cuz well i want to know if you've mentioned this or params 
                if fstuff: 
                        for e in endpoint_candidates:
                                count_string = ''
                                for previous_sibling in e['previous_siblings']:
                                        if type(previous_sibling).__name__=='Tag':
                                                previous_sibling = previous_sibling.text
                                        elif type(previous_sibling).__name__=='NoneType':
                                                previous_sibling = ''
                                        count_string = previous_sibling + '\n'
					if tackone:
						break
                                        #count_candidates.append(previous_sibling + '\n')
                                for next_sibling in e['next_siblings']:
                                        if type(next_sibling).__name__=='Tag':
                                                next_sibling = next_sibling.text
                                        elif type(next_sibling).__name__=='NoneType':
                                                next_sibling = ''
                                        count_string = next_sibling + '\n'
					if tackone:
						break
                                        #count_candidates.append(next_sibling+'\n')
                                count_string = count_string[:-1]
                                count_candidates.append(count_string)
                                #print count_candidates
                                #print "len of count cands magic"
                                #print len(count_candidates)
                                #raw_input("next")

                #count_features = text_features_extraction(count_candidates, v_count, kbest_count)

                return features , count_candidates

        # def text_features_extraction(text, v_count, kbest_count):	
        # 	return v_count.fit_transform(text)
