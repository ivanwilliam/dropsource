import sys
import urllib
from utility_funcs import *
from parameter_funcs_desc import *
from bs4 import BeautifulSoup
from urllib2 import urlopen
from scipy.sparse import coo_matrix, hstack
import glob
import json
import get_api_param_req 
import get_api_param_resp

def load_classifier_api(path):
	clf_data= load_classifier_data(path)
	clf = clf_data[0]
	v = clf_data[1]
	v_count = clf_data[2]
	kbest_count = clf_data[3]
	return clf, v, v_count, kbest_count

def get_highest_prob(prob):
	prob_candidates = []
	for p in prob:
		prob_candidates.append(p[1])
		# print p[1]
	index = prob_candidates.index(max(prob_candidates))	
	return index


def get_api_param_desc_debug(html_file,classifier_loc_name='models/api_parameter_desc_clf.gzip'):
        pf = param_funcs_desc()
        page = urllib.urlopen(html_file).read()

	page = page.replace('<wbr>','')
	page = page.replace('</wbr>','')

	#print 'Loading Classifiers'
	clf, v, v_count, kbest_count = load_classifier_api(classifier_loc_name)

	soup = BeautifulSoup(page, "lxml")
        res_params = get_api_param_resp.get_api_param_resp(html_file)
        req_params = get_api_param_req.get_api_param_req(html_file)
        #print res_params
        #print req_params
	(alln,disc,param_candidates) = pf.extract_candidates(soup)
        fids = [0] * len(disc)

        try:
                candidates_features, count_features_text = pf.features_extraction(param_candidates,disc,[res_params],[req_params],fids)
                X = v.transform(candidates_features)
                prob = clf.predict_proba(X)
                pred = clf.predict(X)
                highest_prob = get_highest_prob(prob)

                result = []
                indexes = []
                for p in range(len(prob)):
                        if not prob[p][1]==0:
                                indexes.append(p)
                threshold = 0.4
                res_p = [d['text'] for d in res_params]
                req_p = [d['text'] for d in req_params]
                combined = []
                pp = None
                np = None
                print "doing stuff"
                secondary_res = []
                ss = []
                for i in indexes:
                        if prob[i][1]>threshold:
                                secondary_res.append(param_candidates[i]['text'])
                                ss.append(param_candidates[i])
                sset = set(secondary_res)
                sset_p = []
                for elem in sset:
                        if not elem in res_p and not elem in req_p:
                                ind = secondary_res.index(elem)
                                sset_p.append(ss[ind])
                #making a dictionary 
                combined_desc = {}
                for sset_p_elem in sset_p:
                        pparam = sset_p_elem['pparam']
                        nparam = sset_p_elem['nparam']
                        if pparam in combined_desc.keys():
                                combined_desc[pparam].append((sset_p_elem['text'].strip(' \n\t\r')).replace('\n',' '))
                        else:
                                combined_desc[pparam]=[(sset_p_elem['text'].strip(' \n\t\r')).replace('\n',' ')]
                for idx,resp in enumerate(res_params):
                        if resp['text'] in combined_desc.keys():
                                res_params[idx]['description'] = combined_desc[resp['text']]
                for idx,resp in enumerate(req_params):
                        if resp['text'] in combined_desc.keys():
                                req_params[idx]['description'] = combined_desc[resp['text']]

                return (combined_desc,alln,disc,param_candidates,candidates_features,prob,res_params,req_params)

        except:
                res = ['No parameter candidate found']
                prob = []
                secres = []
                return (secres,alln,disc,param_candidates,candidates_features,prob,res_params,req_params)

def get_api_param_desc(html_file,classifier_loc_name='models/api_parameter_desc_clf.gzip'):
        (res,alln,disc,param_candidates,cf,ps,resp,reqp) = get_api_param_req_debug(html_file,classifier_loc_name)
        return res

if __name__ == '__main__':
        html_file = sys.argv[1]
        res = get_api_param_desc(html_file)
        print json.dumps(res)
