import re
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
# import ipdb

class param_funcs:
        
        #variables for this class

        label_to_find = 'api_response_parameter_name'  #label for positive samples.
	
        def extract_prev_siblings(self,child,maxsibs=5):
                # lets get the first 3 prev and next siblings
                numsib = 0
                psibs=[]
                for sib in child.parent.previous_siblings:
                        #if not sib == '\n':
                        psibs.append(sib)
                        numsib = numsib+1
                        if(numsib > maxsibs):
                               # print "overflow p"
                                #print len(psibs)
                                #print psibs
                                #raw_input("next")
                                break
                if len(psibs) == 0:
                        #go to parents parent 
                        cp = child.parent 
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.previous_siblings:
                                        psibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                                #psibs = extract_prev_siblings(cp,maxsibs)
                        #if len(psibs) == 0:
                         #       global pzerocount
                         #       pzerocount = pzerocount + 1
        #                print "0 siblings" 

                return psibs 
        def extract_next_siblings(self,child,maxsibs=5):
                numsib = 0
                nsibs=[]
                for sib in child.parent.next_siblings:
         #               if not sib == '\n':
                        nsibs.append(sib)
                        numsib = numsib+1 
                        if(numsib>maxsibs):
                              #  print "overflow n"
                                break
                if len(nsibs) == 0:
                        cp = child.parent
                        if getattr(cp,"parent",None):
                                for sib in cp.parent.next_siblings:
                                        nsibs.append(sib)
                                        numsib = numsib+1
                                        if(numsib > maxsibs):
                                                break
                        #if len(nsibs) == 0:
                         #       global nzerocount
                         #       nzerocount = nzerocount + 1
               #         print "0 n siblings"
                return nsibs


        def depth_first_traversal(self,soup_body,leafnodes):
                #mark that bit as visited 
                #then go ahead and visit all its unvisted childre
                try:
                        isvisted = getattr(soup_body,"visited",False)
                        if not isvisted:
                                soup_body.visited = True
                                haschildren = getattr(soup_body,"children",False)
                                if not haschildren:
                                    if not soup_body.isspace():
                                       # print soup_body 
                                        leafnodes.append(soup_body)

                                if haschildren:
                                        for soup_child in soup_body.children:
                                                self.depth_first_traversal(soup_child,leafnodes)
                except:
                    pass
                    # print "error"

        def get_parent_attr(self,node):
            attrs={}
            try:
                attrs = getattr(node.parent,"attrs",{})
                while not attrs:
                    node = node.parent
                    attrs = getattr(node.parent,"attrs",{})
            except:
                pass
            return attrs


        def extract_candidates(self,soup):
                all_nodes_labels = []
                endpoint_candidates = []
                y=[]
                html_text = soup.findAll(text=True)
                html_text = ' \n '.join(html_text)
                tag_name = ''
                attrs_main = {}
		possible_tags = ['tr','td','table','div','span']
                leafnodes_l = []
        
                self.depth_first_traversal(soup,leafnodes_l)
                if leafnodes_l is not None:
                #for each leaf node 
                #to account for ... if that word is by itself and it says optional or required 
                #it is NOT what we want. 
                        for leaf in leafnodes_l:
                                name = getattr(leaf,"name",None)
                                attrs = getattr(leaf,"attrs",{})
                                if name is None:
                                    #then we want the name of the parent 
                                        if getattr(leaf,"parent",None):
                                                tag_name = leaf.parent.name
                                        else:
                                                tag_name = ''
                                if not attrs:
                                        attrs = self.get_parent_attr(leaf)
                                toappend = False
                                #print "dont append yet"
                                #so lets go ahead and check whether this node is just optional or required 
                                opt_req = re.compile('\[*required\]*|\[*optional\]*')
                                has_opt_req = opt_req.match(leaf)

                                #lets extract useful information from these guys now 
                                #print leaf
                                #raw_input("next")
                                #try:
                                 #       cp = attrs['class']
                                 #       if cp[0] == 'param':
                                 #               print "here"
                                                
                                 #               ipdb.set_trace()
                                #except:
                                #        pass

                                #if has_opt_req:
                                #        print "has the words req"
                                #        ipdb.set_trace()
                                        
                                if tag_name in possible_tags:
                                        try:
                                                if self.label_to_find in attrs['class'] or 'param' in attrs['class']:
                                                        if has_opt_req:
                                                                y.append(0)
                                                        else:
                                                                y.append(1)
                                                else:
                                                        y.append(0)
                                        except:
                                                y.append(0)
                                        toappend = True
                                 #       print "append"
                                else:
                                        xleaf = leaf.strip()
                                        m = re.search('^[A-z,\[\],_]+$',xleaf)
                                        
                                        if m:
                                                try:
                                                        if self.label_to_find in attrs['class'] or 'param' in attrs['class']:
                                                                y.append(1)
                                                        else:
                                                                y.append(0)
                                                except:
                                                        y.append(0)
                                                toappend = True
                                  #      print "append"
                                if toappend:
                                        endpoint_candidates.append({
                                                        'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)
                                        })
                                all_nodes_labels.append({'attrs':attrs,
                                                        'tag':tag_name,
                                                        'text':leaf,
                                                        'parent':leaf.parent,
                                                        'html_text':html_text,
                                                        'previous_siblings':self.extract_prev_siblings(leaf),
                                                        'next_siblings':self.extract_next_siblings(leaf)})

                else:
                    pass
                        # print "List is not iterable"
                if len(y) == len(endpoint_candidates):
                    pass
                        # print "all good so far"


                return (all_nodes_labels,y,endpoint_candidates)



        def features_extraction(self,endpoint_candidates):
                features = []
                largest_occurence = 0
                for e in endpoint_candidates:
                        #ilovefunctions 
                        #pythoncomments=hashtags:P

                        f={}
                        tag = e['tag']
                        parent = e['parent']
                        text = e['text']
                        html_text = e['html_text']
                        previous_sibling = e['previous_siblings']
                        if len(previous_sibling) > 0:
                                previous_sibling = previous_sibling[0]
                        next_sibling = e['next_siblings']
                        if len(next_sibling) > 0:
                                next_sibling = next_sibling[0]

                        f['has_tag_%s'%tag] = 1

                        try:
                                classes = e['attrs']['class']
                                for c in classes:
                                        if c==self.label_to_find:
                                                pass
                                        else:
                                                f['has_class_%s'%c] = 1
                        except:
                                pass

                        #so this is useful we want to find the lenght of 
                        #the string under consideration 
                        
                        if len(text)<10:
                                f['length_smaller_than_10']=1
                        elif len(text)>10 and len(text)<20:
                                f['length_in_between_10_and_20'] = 1
                        elif len(text)>20 and len(text)<30:
                                f['length_in_between_20_and_30'] = 1
                        elif len(text)>30 and len(text)<40:
                                f['length_in_between_30_and_40'] = 1
                        elif len(text)>40 and len(text)<50:
                                f['length_in_between_40_and_50'] = 1
                        elif len(text)>50 :
                                f['length_greater_than_50'] = 1

                        #parameters should not have slashes 
                        #> slashes = < likelihood of being a param 
                        number_of_slash = text.count('/')
                        if number_of_slash<3:
                                f['slashes_less_than_3'] = 1
                        elif number_of_slash>3 and number_of_slash<6:
                                f['slashes_in_between_3_and_6'] = 1
                        elif number_of_slash>6 and number_of_slash<9:
                                f['slashes_in_between_6_and_9'] = 1
                        elif number_of_slash>9 and number_of_slash<15:
                                f['slashes_in_between_9_and_15'] = 1
                        elif number_of_slash>15:
                                f['slashes_greater_than_15'] = 1
                                
                        #lets look for the words query or parameter in this section 
                        if parent.text.lower().find('query') or parent.text.lower().find('parameter')>=0:
                                f['parent_contains_query_param'] = 1
                        else:
                                f['parent_contains_query_param']=0
                        if parent.text.lower().find('response') >=0:
                                f['parent_contains_response']=1
                        else:
                                f['parent_contains_response']=0

                        #lets also look for query or parameter for prev siblings 
                        has_words_query_param = 0 #default 
                        word_dist = 100 #some super high value here 
                        has_word_resp = 0 
                        resp_word_dist = 100
			for idx,psib in enumerate(e['previous_siblings']):
				try:
					if psib.text.lower.find('query') or psib.text.lower.find('parameter')>=0:
						has_words_query_param = 1
                                                word_dist = idx
						#break
                                        if psib.text.lower.find('response') >=0:
                                                has_word_resp = 1
                                                resp_word_dist = idx
				except:
					pass
                        f['psib_contains_query_param'] = has_words_query_param
                        f['psib_contains_resp']=has_word_resp
                        if(word_dist <= 5): #kind of like assuming at most i'll have 5 parameters for something 
                                f['query_param_less_than_5_psibs_away']=1
                        else:
                                f['query_param_less_than_5_psibs_away']=0
			if (resp_word_dist <= 5):
                                f['resp_param_less_than_5_psibs_away']=1
                        else:
                                f['resp_param_less_than_5_psibs_away']=0

                        #lets also look for the words optional or required around this 
                        has_optional_required = 0 #default 
                        for psib in e['next_siblings']:
                                try:
                                        if psib.text.lower.find('optional') or psib.text.lower.find('required')>=0:
                                                has_optional_required=1
                                                break
                                except:
                                        pass
                        f['has_optional_required']=1
                        
                        has_req_opt = re.compile('\[*required\]*|\[*optional\]*')
                        m = has_req_opt.findall(text)
                        if m:
                                f['has_req_opt']=1

                        if type(previous_sibling).__name__ == 'Tag':			
                                if previous_sibling.name.find('h')>=0:
                                        f['previous_sibling_contains_header_tag'] = 1
                                if len(previous_sibling.text.split(' '))<5:
                                        f['previous_sibling_text_less_than_five'] = 1
                        elif type(previous_sibling).__name__ == 'NavigableString':
                                if len(previous_sibling.split(' '))<5 and not previous_sibling.strip()=='':
                                        f['previous_sibling_text_less_than_five'] = 1

			#if the text is a single word 
			sw = re.compile('^[A-z,_,\[\]]+$') 
			m = sw.findall(text)
			if m:
				#looks like it has a single word 
				f['single_word'] = 1
			else:
				f['single_word'] = 0
                        
                        #so lets say with each param theres the likelihood that the surrounding text will actually be a description 
                        #or after a few params you'll find a description 
                        #this means that the word lenght should be say greater than 10 or 7 
                        #the number is fairly arbritrary so we should maybe just group it for all siblings 
                        
                        sum_psib_lens = 0
                        for psib in e['next_siblings']:
                                try:
                                        sum_psib_lens = sum_psibs_lens + len(psib.text.split(' '))
                                except:
                                        pass
                        if sum_psib_lens < 5:
                                f['surrounding_len_smaller_than_5']=1
                        elif sum_psib_lens >=5 and sum_psib_lens < 15:
                                f['surrounding_len_between_5_and_15'] = 1
                        else:
                                f['surroundind_len_greater_than_15'] = 1
                                


                        features.append(f)


                count_candidates = []
		fstuff = True
		tackone = False #now i'm tacking on multiple ones cuz well i want to know if you've mentioned this or params 
                if fstuff: 
                        for e in endpoint_candidates:
                                count_string = ''
                                for previous_sibling in e['previous_siblings']:
                                        if type(previous_sibling).__name__=='Tag':
                                                previous_sibling = previous_sibling.text
                                        elif type(previous_sibling).__name__=='NoneType':
                                                previous_sibling = ''
                                        count_string = previous_sibling + '\n'
					if tackone:
						break
                                        #count_candidates.append(previous_sibling + '\n')
                                for next_sibling in e['next_siblings']:
                                        if type(next_sibling).__name__=='Tag':
                                                next_sibling = next_sibling.text
                                        elif type(next_sibling).__name__=='NoneType':
                                                next_sibling = ''
                                        count_string = next_sibling + '\n'
					if tackone:
						break
                                        #count_candidates.append(next_sibling+'\n')
                                count_string = count_string[:-1]
                                count_candidates.append(count_string)
                                #print count_candidates
                                #print "len of count cands magic"
                                #print len(count_candidates)
                                #raw_input("next")

                #count_features = text_features_extraction(count_candidates, v_count, kbest_count)

                return features , count_candidates

        # def text_features_extraction(text, v_count, kbest_count):	
        # 	return v_count.fit_transform(text)
