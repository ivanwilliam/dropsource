import get_api_param_req
import get_api_param_resp
import get_api_param_desc
import json 
import sys


if __name__=='__main__':
    # html_file = sys.argv[1]
    html_file = '../Labelled html files/v1_estimates_price Developer Platform | Uber.html'
    html_file = '../Labelled html files/GET statuses_user_timeline | Twitter Developers.html'

    #res_resp = get_api_param_resp.get_api_param_resp(html_file)
    #res_req = get_api_param_req.get_api_param_req(html_file)
    (res_desc,alln,disc,param_candidates,cf,ps,res_resp,res_req) = get_api_param_desc.get_api_param_desc_debug(html_file)
    print "Request Params"
    print json.dumps(res_req)

    res_resp_new = []
    x = [res_resp_new.append(item) for item in res_resp if item not in res_resp_new]
    print "Response Params" 
    print json.dumps(res_resp_new)


    res_desc_1 = [x for x in res_desc if len(x.strip().split(' '))>1]
    res_desc_2 = [x for x in res_desc_1 if not x.lower().find('example')==0]
    res_desc_new = []
    x = [res_desc_new.append(item) for item in res_desc_2 if item not in res_desc_new]
    print "Response Descriptions"
    print json.dumps(res_desc_new)
