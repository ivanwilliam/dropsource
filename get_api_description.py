import glob
from bs4 import BeautifulSoup
from urllib2 import urlopen
import urllib
import re
from utility_funcs import *
from description_funcs import *
from scipy.sparse import coo_matrix, hstack

def load_classifier_description(path):
	clf_data= load_classifier_data(path)
	clf = clf_data[0]
	v = clf_data[1]
	v_count = clf_data[2]
	kbest_count = clf_data[3]
	return clf, v, v_count, kbest_count


def get_highest_prob(prob):
	prob_candidates = []
	for p in prob:
		prob_candidates.append(p[1])
	index = prob_candidates.index(max(prob_candidates))	
	return index


if __name__ == '__main__':

	# page = sys.argv[1]

	page = urllib.urlopen('Labelled html files/POST statuses_update | Twitter Developers.html').read()		
	page = urllib.urlopen('html_files/Twilio Cloud Communications - APIs for Voice, VoIP, and Text Messaging.html').read()
	page = urllib.urlopen('html_files/Sign In with LinkedIn | Documentation.html').read()
	page = urllib.urlopen('html_files/Share on LinkedIn | LinkedIn Developer Network.html').read()
	page = urllib.urlopen('Labelled html files/test.html').read()	

	page = page.replace('<wbr>','')
	page = page.replace('</wbr>','')

	# print 'Loading Classifiers'
	# clf, v, kbest = load_classifier_description('models/api_description_clf_randomforest.gzip')
	# soup = BeautifulSoup(page, "lxml")
	# print 'Extracting Candidates'
	# desc_candidates = extract_description_candidates(soup)
	# print 'Extracting Features . . .'
	# # features_extraction(desc_candidates)

	# X = v.transform(desc_candidates) 
	# X = kbest.transform(X)
	# pred = clf.predict(X.toarray())
	# prob = clf.predict_proba(X.toarray())

	# highest_prob = get_highest_prob(prob)
	# if prob[highest_prob][1]==0:
	# 	print 'No Highest Prob candidate found'
	# 	# return 0
	# else:
	# 	print desc_candidates[highest_prob]
	# 	# return endpoint_candidates[highest_prob]['text']

	print 'Loading Classifier . . .'
	clf, v, v_count, kbest_count = load_classifier_description('models/api_description_clf_new(22-3-16).gzip')
	pf = param_funcs()
	print 'Extracting Candidates . . .'
	soup = BeautifulSoup(page, "lxml")
	(alln,disc,desc_candidates_new) = pf.extract_candidates(soup)
	print 'Extracting Features . . .'
	features = features_extraction(desc_candidates_new)
	text = []
	for dcn in desc_candidates_new:
		text.append(dcn['text'])

	X = v.transform(features)
	X_count = v_count.transform(text) 
	X_count = kbest_count.transform(X_count)
	combined_features = hstack((X, X_count),format='csr')
	prob = clf.predict_proba(combined_features)
	pred = clf.predict(combined_features)
	highest_prob = get_highest_prob(prob)

	if prob[highest_prob][1]==0:
		print 'No Highest Prob candidate found'
		# return 0
	else:
		print desc_candidates_new[highest_prob]['text']
		# return endpoint_candidates[highest_prob]['text']

